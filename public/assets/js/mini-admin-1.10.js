function mini_go_url(url) {
    var a = $("<a href='" + url + "' target='_blank'>Apple</a>").get(0);

    var e = document.createEvent('MouseEvents');

    e.initEvent('click', true, true);
    a.dispatchEvent(e);
    console.log('event has been changed');
}

function mini_model_code(url, id){
    Dcat.Form({
        form: '#'+id,
        success: function (response) {
            if (! response.status) {
                Dcat.error(response.data.message);
                return false;
            }

            // Dcat.success(response.data.message);
            var res_html = response.html;

            res_html = res_html.trim();
            if(res_html){
                layer.open({
                    type: 1,
                    area: ['900px', '1000px'],
                    content: '<pre>'+res_html+'</pre>',
                    btn: ['下载文件'],
                    yes: function(index, layero){
                        mini_go_url(url);
                    }
                });
            }else{
                Dcat.warn('弹窗失败');
                return false;
            }

            return false;
        },
        error: function () {
            // 非200状态码响应错误
        }
    });
};
