<p align="center"><a href="" target="_blank"><img  src="https://img.mini.minisns.cn/images/logo/minilogo.svg" width="200"></a></p>

## 关于MiniSNS
#### MiniSNS 专注于社区带货   
   
![](https://img.mini.minisns.cn/images/hbx/h/update_tip.jpg)   
**关于作者**：本人工作岗位是NLP工程师，俗称炼丹侠的ai工程师一种，所以我擅长的是逻辑，算法，后端，并不擅长前端的UI美化，这里真诚的邀请前端大佬和我一起打磨这个产品。我一直想做一个可以永远运营下去的App，无关赚钱，只是读书时候的理想。   
目前市面上真正完善运营级的社交程序不多，很难满足自己的要求，那我们就创造一个吧。   
**关于代理**：有两种方式，具体请看[https://doc.minisns.cn/doc/99/](https://doc.minisns.cn/doc/99/ "https://doc.minisns.cn/doc/99/")   


### 一、演示
   
1、安卓APP端：  [https://down.minisns.cn/app.apk](https://down.minisns.cn/app.apk "https://down.minisns.cn/app.apk")   
2、IOS appstore搜索“宠也”   
3、小程序端：微信小程序搜索：宠也网  
4、pc端：[https://www.minisns.cn](https://www.minisns.cn "https://www.minisns.cn")   
![](https://img.mini.minisns.cn/images/hbx/h/minipro.jpg)  
5、后台：[https://test.minisns.cn/admin](https://test.minisns.cn/admin "https://test.minisns.cn/admin")  
账号：admin  
密码：minisns666  
### 二、费用

| 类型 | 高级源码授权 | 源码授权 | 普通授权 |
| --- | --- | --- | --- |
| 费用 | 6w/永久 | 1.5w/永久 | 900/永久/单域名 |
| 方式 | 提供后端源码，即使你二开了，也包更新新功能 | 提供后端源码，不包更新 | 提供后端加密编译后源码，消耗性能低，甚至响应更快 |
| 功能 | 完全相同 | 完全相同 | 完全相同 |
| 前端 | 完全开源 | 完全开源 | 完全开源 |

### 三、截图

| 界面 | 界面 |
| --- | --- |
| ![](https://img.mini.minisns.cn/hbx/h/0.jpg) | ![](https://img.mini.minisns.cn/hbx/h/1.jpg) |
| ![](https://img.mini.minisns.cn/hbx/h/2.jpg) | ![](https://img.mini.minisns.cn/hbx/h/3.jpg) |
| ![](https://img.mini.minisns.cn/hbx/h/4.jpg) | ![](https://img.mini.minisns.cn/hbx/h/5.jpg) |


### 四、开始
1、整个项目分为前端和后端，后端地址：[https://gitee.com/wen-open/mini_back](https://gitee.com/wen-open/mini_back "https://gitee.com/wen-open/mini_back")  
2、前端文档：[https://doc.minisns.cn/project-2/](https://doc.minisns.cn/project-2/ "https://doc.minisns.cn/project-2/")  
后端文档：[https://doc.minisns.cn/project-1/](https://doc.minisns.cn/project-1/ "https://doc.minisns.cn/project-1/")  
3、本项目前端是基于“轻航”的微信小程序源码重写为uniapp代码二开而来，轻航官网：[https://qinghang.supengjun.com/index.html](https://qinghang.supengjun.com/index.html "https://qinghang.supengjun.com/index.html")   
4、进项目交流群请加我微信：ShaoWenSir  
![](https://img.mini.minisns.cn/images/mmqrcode1677495496626%281%29.png)  

### 五、开源致谢（排名不分先后）
laravel   
uniapp   
elastic search   
dcat_admin   
linui   
gateway work (workerman)   
mp_html   
轻航小程序   
scss   
laravel-admin   
...