/*
 Navicat Premium Data Transfer

 Source Server         : mini-new
 Source Server Type    : MySQL
 Source Server Version : 50718
 Source Host           : sh-cynosdbmysql-grp-2g618yuq.sql.tencentcdb.com:21245
 Source Schema         : product_mini_chongyeapp_20221112

 Target Server Type    : MySQL
 Target Server Version : 50718
 File Encoding         : 65001

 Date: 16/09/2023 22:23:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wx_menu
-- ----------------------------
DROP TABLE IF EXISTS `wx_menu`;
CREATE TABLE `wx_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT 0 COMMENT '父级菜单ID',
  `order` int(11) NOT NULL DEFAULT 0 COMMENT '排序',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '菜单',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '图标',
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'URL',
  `depth` tinyint(4) NOT NULL DEFAULT 1 COMMENT '层级',
  `created_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated_at` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_0`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '全局菜单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of wx_menu
-- ----------------------------
INSERT INTO `wx_menu` VALUES (1, 0, 1, 'PC导航头部菜单', NULL, NULL, 1, '2022-09-16 09:31:57', '2022-09-16 15:44:57');
INSERT INTO `wx_menu` VALUES (2, 0, 8, '认证&合作', NULL, NULL, 1, '2022-09-16 09:52:06', '2022-09-17 15:20:22');
INSERT INTO `wx_menu` VALUES (3, 0, 14, '赞助商&技术支持', NULL, NULL, 1, '2022-09-16 09:52:49', '2022-09-17 15:20:40');
INSERT INTO `wx_menu` VALUES (4, 0, 22, '菜单4', NULL, NULL, 1, '2022-09-16 09:54:25', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (5, 0, 23, '菜单5', NULL, NULL, 1, '2022-09-16 09:54:50', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (6, 0, 24, '菜单6', NULL, NULL, 1, '2022-09-16 09:55:10', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (7, 0, 25, '菜单7', NULL, NULL, 1, '2022-09-16 09:55:31', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (8, 0, 26, '菜单8', NULL, NULL, 1, '2022-09-16 09:55:51', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (9, 0, 27, '菜单9', NULL, NULL, 1, '2022-09-16 09:56:25', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (10, 0, 28, '菜单10', NULL, NULL, 1, '2022-09-16 09:56:48', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (11, 0, 29, '菜单11', NULL, '', 1, '2022-09-16 15:13:24', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (12, 0, 30, '菜单12', NULL, '', 1, '2022-09-16 15:14:50', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (13, 0, 31, '菜单13', NULL, '', 1, '2022-09-16 15:15:11', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (14, 0, 32, '菜单14', NULL, '', 1, '2022-09-16 15:15:32', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (15, 0, 33, '菜单15', NULL, NULL, 1, '2022-09-16 15:39:41', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (16, 0, 34, '菜单16', NULL, NULL, 1, '2022-09-16 15:40:00', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (17, 0, 35, '菜单17', NULL, NULL, 1, '2022-09-16 15:41:02', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (18, 0, 36, '菜单18', NULL, NULL, 1, '2022-09-16 15:41:17', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (19, 0, 37, '菜单19', NULL, NULL, 1, '2022-09-16 15:41:30', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (20, 0, 38, '菜单20', NULL, NULL, 1, '2022-09-16 15:41:42', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (21, 0, 39, '菜单21', NULL, NULL, 1, '2022-09-16 15:41:53', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (22, 0, 40, '菜单22', NULL, NULL, 1, '2022-09-16 15:42:03', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (23, 0, 41, '菜单23', NULL, NULL, 1, '2022-09-16 15:42:12', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (24, 0, 42, '菜单24', NULL, NULL, 1, '2022-09-16 15:42:23', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (25, 0, 43, '菜单25', NULL, NULL, 1, '2022-09-16 15:42:33', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (26, 0, 44, '菜单26', NULL, NULL, 1, '2022-09-16 15:42:43', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (27, 0, 45, '菜单27', NULL, NULL, 1, '2022-09-16 15:42:52', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (28, 0, 46, '菜单28', NULL, NULL, 1, '2022-09-16 15:43:02', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (29, 0, 47, '菜单29', NULL, NULL, 1, '2022-09-16 15:43:13', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (30, 0, 48, '菜单30', NULL, NULL, 1, '2022-09-16 15:43:34', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (31, 1, 2, '首页', 'fa-address-card', '/', 1, '2022-09-16 15:43:57', '2022-09-16 19:58:13');
INSERT INTO `wx_menu` VALUES (32, 1, 3, '热榜', NULL, '/hot', 1, '2022-09-16 15:44:08', '2022-09-16 17:21:45');
INSERT INTO `wx_menu` VALUES (33, 1, 4, '圈子', NULL, '/circles', 1, '2022-09-16 15:44:15', '2022-09-16 18:16:59');
INSERT INTO `wx_menu` VALUES (34, 1, 5, '标签', NULL, '/tag', 1, '2022-09-16 15:46:02', '2022-09-16 15:46:09');
INSERT INTO `wx_menu` VALUES (35, 1, 6, '神器导航', 'fa-angellist', '/tool', 1, '2022-09-16 19:22:13', '2022-09-16 19:53:06');
INSERT INTO `wx_menu` VALUES (36, 35, 7, '设计', NULL, '/tool/21', 1, '2022-09-16 19:25:05', '2022-09-16 19:53:06');
INSERT INTO `wx_menu` VALUES (37, 2, 9, '账号认证', NULL, NULL, 1, '2022-09-17 14:59:41', '2022-09-17 15:00:57');
INSERT INTO `wx_menu` VALUES (38, 2, 10, '广告/赞助', NULL, NULL, 1, '2022-09-17 15:00:06', '2022-09-17 15:00:57');
INSERT INTO `wx_menu` VALUES (39, 2, 11, '创作者入驻', NULL, NULL, 1, '2022-09-17 15:00:19', '2022-09-17 15:00:57');
INSERT INTO `wx_menu` VALUES (40, 2, 12, '成为运营者', NULL, NULL, 1, '2022-09-17 15:00:37', '2022-09-17 15:00:57');
INSERT INTO `wx_menu` VALUES (41, 2, 13, '提供技术支持', NULL, NULL, 1, '2022-09-17 15:00:48', '2022-09-17 15:00:57');
INSERT INTO `wx_menu` VALUES (42, 3, 15, '宠也', NULL, 'https://minisns.cn', 1, '2022-09-17 18:22:17', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (43, 3, 16, '腾讯云', NULL, NULL, 1, '2022-09-17 18:22:36', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (44, 3, 17, 'Dcat-admin', NULL, NULL, 1, '2022-09-17 18:23:09', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (45, 3, 18, 'Laravel', NULL, NULL, 1, '2022-09-17 18:23:21', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (46, 3, 19, 'Linui', NULL, NULL, 1, '2022-09-17 18:23:31', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (47, 3, 20, 'Bootstrap', NULL, NULL, 1, '2022-09-17 18:24:14', '2023-01-07 15:59:34');
INSERT INTO `wx_menu` VALUES (48, 3, 21, '宝塔Linux面板', NULL, NULL, 1, '2022-09-17 18:24:32', '2023-01-07 15:59:34');

SET FOREIGN_KEY_CHECKS = 1;
