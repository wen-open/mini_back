// 分享笔记方法
const tapShare = function(obj) {
	let that = this;
	that.postsIndex = obj.index;
	that.showSharePopup = Date.now();
	that.postsId = obj.post_id;
	return false;
};

const tapCollect = function(obj){
	let that = this;
	uni.wen.util.doVibrateShort();
	
	let posts = that.posts;
	
	for ( let i = 0; i < posts.length; i ++ ) {
		if(posts[i].id == obj.post_id ){
	
			if (posts[i].is_collect) {
				posts[i].collected_count -= 1;
			} else {
				posts[i].collected_count += 1;
			}
			posts[i].is_collect = !posts[i].is_collect;
			
			break;
		}
	}
	
	that.posts = posts;
	
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'posts/process', {
			type: 'collect',
			posts_id: obj.post_id
		},
		'POST'
	).then(function(res) {
		uni.loading(false);
		if (res.status) {
			
		}else{
			let posts = that.posts;
			
			for ( let i = 0; i < posts.length; i ++ ) {
				if(posts[i].id == obj.post_id ){
			
					if (posts[i].is_collect) {
						posts[i].collected_count += 1;
					} else {
						posts[i].collected_count -= 1;
					}
					posts[i].is_collect = !posts[i].is_collect;
					
					break;
				}
			}
			
			that.posts = posts;
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});
		}
	});
};


// 笔记喜欢接口
const taplikes = function(obj) {
	let that = this;
	uni.wen.util.doVibrateShort();
	
	let posts = that.posts;
	
	for (let index = 0; index < posts.length; index ++) {
	    if (posts[index].id == obj.post_id) {
			if (posts[index].is_like) {
				posts[index].like_count -= 1;
			} else {
				posts[index].like_count += 1;
			}
			
			posts[index].is_like = !posts[index].is_like;
			break;
	    }
	}

	that.posts = posts;
	
	
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'posts/process', {
			type: 'like',
			posts_id: obj.post_id
		},
		'POST'
	).then(function(res) {
		uni.loading(false);
		if (res.status) {

		}else{
			
			let posts = that.posts;
			
			for (let index = 0; index < posts.length; index ++) {
			    if (posts[index].id == obj.post_id) {
					if (posts[index].is_like) {
						posts[index].like_count += 1;
					} else {
						posts[index].like_count -= 1;
					}
					
					posts[index].is_like = !posts[index].is_like;
					break;
			    }
			}
			
			that.posts = posts;
			
		}
	});
};

// 展开更多
const unfoldTap = function (postIndex) {
  let that = this;
  let posts = that.posts;
  posts[postIndex].is_content_beyond = false;

  that.posts = posts;
}

const soundsPlayCall = function(post_id){
	let that = this;
	let needFresh = false;
	for (let index = 0; index < that.posts.length; index ++) {
	    if (that.posts[index].id != post_id) {
			if( that.posts[index].sounds && that.posts[index].sounds.length > 0 ){
				for( let s_index = 0; s_index < that.posts[index].sounds.length; s_index++ ){
					if (that.posts[index].sounds[s_index]['innerAudioContext']) {
					  that.posts[index].sounds[s_index]['isPlay'] = false;
					  that.posts[index].sounds[s_index]['innerAudioContext'].pause();
					  needFresh = true;
					}
				}
			}
	    }
	}

	if(needFresh){
		that.$forceUpdate();
	}
}

const payPost = function(){
	let that = this;
	let posts = that.posts;
	if(posts[that.postsIndex].pay_obj.pay_type == 1){
		let paywaylist = [0];
		if(that.$store.state.config.app.coins && that.$store.state.config.app.coins.pay == true){
			paywaylist.push(3)
		}
		that.shutPayContent();
		let price = posts[that.postsIndex].pay_obj.price;
		if(that.$store.state.config.app.vip.discount > 0 && that.$store.state.userInfo.is_member){
			price = (price * ((100 - that.$store.state.config.app.vip.discount) / 100)).toFixed(2);
		}
		that.$emit('toast', {
			type:'payway',
			content: '购买付费',
			price: price,
			isVipPrice: that.$store.state.userInfo.is_member,
			timeout: 2000,
			isClick:true,
			paywaylist: paywaylist,
			order: {
				type: 4,
				post_id: that.postsId,
				scene: that.$store.state.scene,
				postsIndex: that.postsIndex
			}
		});
	}else{
		uni.loading(true);
		uni.wen.util.request(
			uni.wen.api.ApiRootUrl + 'posts/pay', {
				post_id: that.postsId,
				scene: that.$store.state.scene
			},
			'POST'
		).then(function(res) {
			uni.loading(false);
			uni.$store.commit('popopChange', false);
			that.payContentPopup = false;
			if (res.code == 200) {
				let posts = that.posts;
				posts[that.postsIndex] = res.data[0];
				that.posts = posts;
				
				if(that.$store.state.scene == 9){
					that.imgItem.is_pay = 0;
				}
				
				setTimeout(function(){
					uni.showToast({
						title: '购买成功',
						icon: 'none',
						duration: 1500
					});
				}, 500);

			}else if(res.code == 200011){
				// #ifdef H5
				uni.showToast({
					title: uni.$store.state.config.app.coins.name + '不足！',
					icon: 'none'
				});
				return;
				// #endif
				if(that.$store.state.config.pays.paycode_14){
					let every_coin = 0;
					if(that.$store.state.config.app.coins && that.$store.state.config.app.coins.reward){
						// #ifdef MP
						every_coin = that.$store.state.config.app.coins.reward.mp.every;
						// #endif
						// #ifndef MP
						every_coin = that.$store.state.config.app.coins.reward.uni.every;
						// #endif
					}
					if(every_coin && every_coin > 0){
						uni.showModal({
							title:  uni.$store.state.config.app.coins.name + '不足！',
							content: '观看激励视频可获得' + every_coin + uni.$store.state.config.app.coins.name + '/次，是否观看？',
							confirmText: "获取",
							cancelText: "算了",
							success: function(res) {
								if (res.confirm) {
									uni.wen.toUrl(6, '/pagesA/mine/earnings/inspire/inspire', null);
								} else if (res.cancel) {

								}
							}
						});
					}else{
						uni.showToast({
							title: uni.$store.state.config.app.coins.name + '不足！',
							icon: 'none'
						});
					}
				}else{
					uni.showToast({
						title: uni.$store.state.config.app.coins.name + '不足！',
						icon: 'none'
					});
				}
			}else if(res.code == 200012){
				uni.showToast({
					title: res.message,
					icon: 'none',
					duration: 1500
				});
				setTimeout(function(){
					uni.wen.toUrl(6, "/pagesA/mine/earnings/recharge/recharge", null);
				}, 1000);

			}else {
				uni.showToast({
					title: res.message,
					icon: 'none',
					duration: 1500
				});
			}

		});
	}
}

const showPayContent = function(postIndex){
	let that = this;
	let vip_price = -1;
	let price =  that.posts[postIndex].pay_obj.price;
	if(that.$store.state.config.app.vip.discount > 0 && that.$store.state.userInfo.is_member){
		if(that.posts[postIndex].pay_obj.pay_type == 1){
			vip_price = (price * ((100 - that.$store.state.config.app.vip.discount) / 100)).toFixed(2);
		}else{
			vip_price = Math.floor(price * ((100 - that.$store.state.config.app.vip.discount) / 100));
		}
	}
	let payContentObj = {
		"price": that.posts[postIndex].pay_obj.price,
		"pay_type": that.posts[postIndex].pay_obj.pay_type,
		"credit": that.posts[postIndex].pay_obj.credit,
		"rest": (100 - that.posts[postIndex].pay_obj.words_percent),
		"has_hidden": that.posts[postIndex].pay_obj.has_hidden,
		"has_file": that.posts[postIndex].pay_obj.has_file,
		"has_img": that.posts[postIndex].pay_obj.has_img,
		"has_video": that.posts[postIndex].pay_obj.has_video,
		"vip_price": vip_price
	}

	that.payContentPopup = !that.payContentPopup,
	that.postsId = that.posts[postIndex].id,
	that.postsUserId = that.posts[postIndex].user_id,
	that.postsIndex = postIndex,
	that.payContentObj = payContentObj
	
	uni.$store.commit('popopChange', true);

	if(payContentObj.pay_type == 0){
		let flag = false;
		if(that.$store.state.userInfo.coins){
			if(that.$store.state.userInfo.is_member){
				if(that.$store.state.userInfo.coins < payContentObj.vip_price){
					flag = true;
				}
			}else{
				if(that.$store.state.userInfo.coins < payContentObj.price){
					flag = true;
				}
			}

		}
		if(flag){
			// #ifdef H5
			return;
			// #endif
			if(that.$store.state.config.pays.paycode_14){
				let every_coin = 0;
				if(that.$store.state.config.app.coins && that.$store.state.config.app.coins.reward){
					// #ifdef MP
					every_coin = that.$store.state.config.app.coins.reward.mp.every;
					// #endif
					// #ifndef MP
					every_coin = that.$store.state.config.app.coins.reward.uni.every;
					// #endif
				}
				if(every_coin && every_coin > 0){
					uni.showModal({
						title: uni.$store.state.config.app.coins.name + '不足！',
						content: '观看激励视频可获得' + every_coin + uni.$store.state.config.app.coins.name + '/次，是否观看？',
						confirmText: "获取",
						cancelText: "算了",
						success: function(res) {
							if (res.confirm) {
								uni.wen.toUrl(6, '/pagesA/mine/earnings/inspire/inspire', null);
							} else if (res.cancel) {

							}
						}
					});
				}
			}
		}

	}
};

//打开评论菜单
const tapComments = function(obj) {
	uni.$store.commit('popopChange', true);
	let that = this;
	let postsId = obj.post_id;
	let commentCount = obj.comment_count;
	
	that.showComments = !that.showComments;
	that.commentCount = commentCount;
	that.postsId = postsId;
	that.commentId = '';
	that.replyUserId = '';
	that.replyName = '此时此刻想说~';
};

// 打开评论输入框
const tapComment = function(obj) {
	let that = this;
	let commentId = obj.commentId;
	let replyUserId = obj.replyUserId;
	let replyName = obj.replyName;

	if(commentId){
		if (typeof replyName != 'undefined' && commentId != 'undefined' && replyUserId != 'undefined') {
			replyName = '回复 ' + replyName + '：';
		}else{
			replyName = '此时此刻想说~';
		}
	} else {
		replyName = '此时此刻想说~';
		commentId = '';
		replyUserId = '';
	}
	that.commentId = commentId;
	that.replyUserId = replyUserId;
	that.replyName = replyName;
	that.commentFormShow = Date.now();
	that.showComments = Date.now();
};

const tapReward = function(obj) {
	let that = this;
	
	that.rewardPopup = !that.rewardPopup,
	that.postsId = obj.post_id,
	that.postsUserId = obj.userid,
	that.postsIndex = obj.index,
	that.rewardPrice = '';
	
	uni.$store.commit('popopChange', true);
};

//充电弹窗
const tapGiveCoin = function(obj) {
	let that = this;
	
	that.postsId = obj.post_id;
	that.postsIndex = obj.index;
	
	let child = this.$unicom("#mytotast_" + that.$store.state.scene + '_' + that.$store.state.scene_id);
	if(child){
		child.open({
			type:'givecoin',
			content: '投币',
			timeout: 2000,
			isClick:true,
			mask: 9,
		});
		return false;
	}
};

//关闭充电弹窗
const shutReward = function() {
	let that = this;
	that.rewardPopup = !that.rewardPopup
	uni.$store.commit('popopChange', false);
};

const shutPayContent = function() {
	let that = this;
	that.payContentPopup = !that.payContentPopup;
	uni.$store.commit('popopChange', false);
};

const commentFormMaskClick = function(){
	let that = this;
	that.commentFormShow = 0;
}

const selectVote = function(obj){
	let that = this;
	let postIndex = obj.postIndex;
	let vote_id = obj.vote_id;
	let position = obj.position;
	uni.wen.util.request(uni.wen.api.ApiRootUrl + 'user/vote', {
		vote_id: vote_id, position: position
	}, 'POST').then(function(res) {
		if (res.status) {
			that.posts[postIndex].vote  = res.data;
			uni.showToast({
				title: '投票成功',
				icon: 'none',
				duration: 1500
			});
		}else{
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});
		}
	});
};

const onOpenExceptionalAccountCheck = function(){
	let that = this;

	var reg = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
	let rewardPrice = that.rewardPrice;
	let postsId = that.postsId;
	let postsUserId = that.postsUserId;

	if (!reg.test(rewardPrice)) {
		uni.showToast({
			title: '请输入一个正确的充电金额',
			icon: 'none'
		});
		return
	} else if (rewardPrice < 1 || rewardPrice > 1000) {
		uni.showToast({
			title: '充电金额必须在1-1000',
			icon: 'none'
		});
		return
	}
	uni.$store.commit('popopChange', false);
	that.rewardPopup = false;

	let paywaylist = [0];
	if(that.$store.state.platform == 'ios' && that.$store.state.device == 'app'){
		paywaylist.push(1);
		paywaylist.push(2);
	}else{
		paywaylist.push(1);
		paywaylist.push(2);
	}

	if(that.$store.state.config.app.coins && that.$store.state.config.app.coins.pay == true){
		paywaylist.push(3)
	}
	that.$emit('toast', {
		type:'payway',
		content: '充电',
		price: rewardPrice,
		timeout: 2000,
		isClick:true,
		paywaylist: paywaylist,
		order: {
			type: 2,
			rewardPrice: rewardPrice,
			postsId: postsId,
			postsUserId: postsUserId
		}
	});
};

//关闭评论菜单
const toShutComments = function() {
	let that = this;
	that.showComments = !this.showComments;
	that.postsId = 0;
	that.commentFormShow = 0;
	that.commentId = '';
	that.replyUserId = '';
	that.replyName = '此时此刻想说~';
	
	uni.$store.commit('popopChange', false);
};


//评论Input监听/发送评论
const onInputComment = function(data) {
	let that = this;
	uni.wen.util.doVibrateShort();

	uni.loading(true);

	var value = data.content;
	let imageValue = data.img;
	let giveCoin = data.giveCoin;
	let postsId = Math.abs(that.postsId);
	let commentId = that.commentId;
	let replyUserId = that.replyUserId;
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'comment/add', {
			posts_id: postsId,
			comment_content: value,
			comment_img_url: imageValue,
			comment_id: commentId,
			reply_user_id: replyUserId,
			giveCoin: giveCoin
		},
		'POST'
	).then(function(res) {
		uni.loading(false);
		if (res.status) {

			let posts = that.posts;
			for ( let i = 0; i < posts.length; i ++ ) {
				if(posts[i].id == postsId ){
					if (posts[i].comment_count) {
						posts[i].comment_count += 1;
					} else {
						posts[i].comment_count = 1;
					}
					break;
				}
			}

			that.commentId = '';
			that.replyUserId = '';
			that.replyName = '此时此刻想说~';
			that.commentFormShow = 0;
			that.postsId = (that.postsId * -1);
			that.posts = posts;
			that.commentCount = that.commentCount + 1;
			
			//  模板消息
			// #ifdef MP-WEIXIN
			if(res.data.tmplIds && res.data.tmplIds.length > 0){
				wx.requestSubscribeMessage({
					tmplIds: res.data.tmplIds,
					success (res1) {
						uni.showToast({
							title: res.data.tip,
							icon: 'none',
							duration: 1500,
						});
					},
					fail(err) {
						uni.showToast({
							title: '已拒绝接受订阅消息',
							icon: 'none'
						});
					}
				});
				return false;
			}
			// #endif
			
			uni.showToast({
				title: res.data.tip,
				icon: 'none',
				duration: 1500,
			});
			
		}else if(res.code == 200016) {
			uni.showModal({
					title: res.data.tip[0] ? res.data.tip[0] : '存在违禁词',
					content: res.data.hit_word ? res.data.hit_word.join(' ') : '请勿发布违规内容',
					showCancel: false,
					confirmText: '朕知道了',
					confirmColor: that.primaryColor
				});
        }else if( res.code == 200041 ){
			uni.showModal({
			    title: '默认昵称不能发布内容',
			    content: '为了您在社区的信誉，请勿以"微信用户","普通用户"'+ (that.$store.state.config.user?.default_user_name.length > 0 ? ',"'+that.$store.state.config.user?.default_user_name+'"' : '') +'开头',
			    showCancel: true,
			    confirmText: '修改昵称',
			    confirmColor: that.primaryColor,
			    success(res) {
			        if (res.confirm){
						uni.wen.toUrl(6, '/pagesA/mine/editmine/editmine?name=1', null);
					}
			    }
			});
			return false;
		}else{
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});
		}
	});
};

 //充电列表弹窗
const onClickReward = function(e) {
	let that = this;
	let postsId = e.currentTarget.dataset.id;
	let exceptionalCount = e.currentTarget.dataset.ecount;

	if (typeof postsId != 'undefined' && exceptionalCount != 'undefined') {
		uni.wen.util.request(uni.wen.api.ApiRootUrl + 'posts/getExceptionalList', {
			posts_id: postsId
		}).then(function(res) {
			that.exceptionalList = res.data;
			that.exceptionalCount = exceptionalCount;
		});
	}

	that.rewardDialog = !that.rewardDialog;
	uni.$store.commit('popopChange', false);
};

//充电选择金额
const addRewardPrice = function(e) {
	let that = this;
	that.rewardPrice = e.currentTarget.dataset.price;
};

//充电输入金额
const rewardPriceChange = function(e) {
	let that = this;
	if(that.global__platform__ == 'ios'){
		uni.showToast({
			title: '抱歉，根据IOS平台相关规范，充电功能不允许自定义金额！',
			icon: 'none'
		})
	}else{
		that.rewardPrice = e.detail.value;
	}
};


// 文章组件事件
//笔记点击关注用户
const postsActionFollow = function (e) {
    let that = this;
    let userId = e.currentTarget.dataset.userid;
    uni.wen.util.actionFollow(userId).then((res) => {
        if (res.status) {

            let posts = that.posts;

            for (var v in posts) {
                if (posts[v].user.id == userId) {
                    posts[v].is_follow_user = !posts[v].is_follow_user;
                }
            }

            that.posts = posts;

            uni.showToast({
                title: res.message,
                icon: 'none',
                duration: 1500
            });
        } else {
            uni.showToast({
                title: res.message,
                icon: 'none',
                duration: 1500
            });
        }
    });
};




module.exports = function (obj) {
	obj.tapShare = tapShare;
	obj.tapCollect = tapCollect;
	obj.taplikes = taplikes;
	obj.unfoldTap = unfoldTap;
	obj.soundsPlayCall = soundsPlayCall;
	obj.payPost = payPost;
	obj.showPayContent = showPayContent;
	obj.tapComments = tapComments;
	obj.tapComment = tapComment;
	obj.tapReward = tapReward;
	obj.tapGiveCoin = tapGiveCoin;
	obj.shutReward = shutReward;
	obj.shutPayContent = shutPayContent;
	obj.commentFormMaskClick = commentFormMaskClick;
	obj.selectVote = selectVote;
	obj.onOpenExceptionalAccountCheck = onOpenExceptionalAccountCheck;
	obj.toShutComments = toShutComments;
	obj.onInputComment = onInputComment;
	obj.onClickReward = onClickReward;
	obj.rewardPriceChange = rewardPriceChange;
	obj.addRewardPrice = addRewardPrice;
	obj.postsActionFollow = postsActionFollow;
};
