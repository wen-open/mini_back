const creatForums = function () {
    let that = this;
	
	if(that.isUploading){
		uni.showToast({
			title: '还在上传中，请稍后',
			icon: 'none'
		});
		return false;
	}
	
	if(that.imagetext_save){
		that.imagetext_save();
	}

	if (uni.$store.state.token) {

	} else {
		if(that.saveDraft){
			that.saveDraft();
		}
		setTimeout(function(){
			if(that.$store.state.scene == 14){
				return;
			}
			uni.wen.toUrl(-4, null, null);
		}, 200);
		return false;
	}

	let editer = that.editer;
	let imagetext_content = '';
	if(that.editer == 'imagetext'){
		imagetext_content = that.imagetext_content;
	}

    let textinput = that.textinput; //笔记内容

    let circle_id = that.circle ? that.circle.id : 0; //圈子id

    let tags = that.tags; //话题数组

    let goods = that.goods; //商品数组

    let location = that.location; // 地址信息

    let image_urls = that.image_urls; // 图片地址数组

    let video_url = that.video_url; // 视频地址

    let video_thumb_url = that.video_thumb_url; // 视频封面

    let video_height = that.video_height; // 视频高度

    let video_width = that.video_width; // 视频宽度

	let vote_list = that.isVoteContent ? that.voteInputList : []; // 投票数组

	let file_list = that.upload_files ? that.upload_files : []; // 附件列表

	let sounds_list = that.sounds_files ? that.sounds_files : []; // 附件列表

	let pay_obj = that.pay_content_obj;

	let refer_id = that.refer_id;

	let contact_phone = that.contact_phone;

	let sph_video_feed_token = that.sph_video_feed_token;
	
	let sph_image_urls = that.sph_image_urls;
	
	let posts_title = that.posts_title;
	
	let is_anonymous = that?.is_anonymous ?? 0;
	
	let selectUseds = '';
	if(that?.selectUseds?.length > 0){
		for (let i =0; i < that.selectUseds.length; i ++) {
			if(selectUseds){
				selectUseds += (','+that.selectUseds[i].id);
			}else{
				selectUseds += that.selectUseds[i].id;
			}
		}
	}
	
	let selectBureaus = '';
	if(that?.selectBureaus?.length > 0){
		for (let i =0; i < that.selectBureaus.length; i ++) {
			if(selectBureaus){
				selectBureaus += (','+that.selectBureaus[i].id);
			}else{
				selectBureaus += that.selectBureaus[i].id;
			}
		}
	}
	
	let selectVoter = 0;
	if(that?.selectVoter?.id > 0){
		selectVoter = that.selectVoter.id;
	}
	
	if(refer_id && refer_id > 0){
		
	}else{
		if(that.editer != 'simple' && that.editer != 'imagetext' && that.editer != 'refer'){
			uni.showToast({
				title: '程序运行错误，请反馈一下给管理员',
				icon: 'none'
			});
			return false;
		}
	}
	
	// 付费内容
	if(that.pay_content_obj && that.pay_content_obj.price > 0){
		if( that.pay_content_obj.words_percent == 100 && that.pay_content_obj.is_file == 0 && that.pay_content_obj.is_img == 0 && that.pay_content_obj.is_video == 0 && (that.pay_content_obj.hidden == '' || that.pay_content_obj.hidden == undefined ) ){
			uni.showToast({
			    title: '请合理配置付费内容',
			    icon: 'none'
			});
			return;
		}
		if( that.pay_content_obj.credit_type == 0 ){
			if( that.pay_content_obj.price > that.$store.state.config.page.create.max_coins ){
				uni.showToast({
				    title: '最大支持' + that.$store.state.config.page.create.max_coins + uni.$store.state.config.app.coins.name,
				    icon: 'none'
				});
				return;
			}
		}else if(that.pay_content_obj.credit_type == 1){
			if( that.pay_content_obj.price > that.$store.state.config.page.create.max_blance ){
				uni.showToast({
				    title: '最大支持' + that.$store.state.config.page.create.max_blance + '余额',
				    icon: 'none'
				});
				return;
			}
		}
		if(that.pay_content_obj.is_file == 1 && file_list.length <= 0){
			uni.showToast({
			    title: '还没有上传附件，不可以为付费状态',
			    icon: 'none'
			});
			return;
		}
		if(that.pay_content_obj.is_img == 1 && image_urls.length <= 0){
			uni.showToast({
			    title: '还没有上传图片，不可以为付费状态',
			    icon: 'none'
			});
			return;
		}
		if(that.pay_content_obj.is_video == 1 && !video_url){
			uni.showToast({
			    title: '还没有上传视频，不可以设置付费状态',
			    icon: 'none'
			});
			return;
		}
	}

    if (textinput == '' && imagetext_content == '' && posts_title == '') {
        uni.showToast({
            title: '还没有说点什么呢',
            icon: 'none'
        });
        return;
    }

	if(refer_id && refer_id > 0){

	}else{
		if(that.$store.state.config.page.create.circle_require == 1){
			if (circle_id == '' || circle_id == undefined) {
			    uni.showToast({
			        title: '请选择一个圈子再发布吧',
			        icon: 'none'
			    });
			    return;
			}
		}
	}
	uni.wen.util.doVibrateShort();
	
    uni.loading(true);
    uni.wen.util.request(
        uni.wen.api.ApiRootUrl + 'posts/process',
        {
			type: that?.editId > 0 ? 'update' : 'add',
			posts_id: that?.editId ?? 0,
			editer: editer,
			imagetext_content: imagetext_content,
            posts_content: textinput,
			posts_title: posts_title,
            circle_id: circle_id,
            tags: tags,
            goods: goods,
            address: location,
            image_urls: image_urls,
            video_url: video_url,
            video_thumb_url: video_thumb_url,
            video_height: video_height,
            video_width: video_width,
			vote_list: vote_list,
			file_list: file_list,
			sounds_list: sounds_list,
			pay_obj: pay_obj,
			refer_id: refer_id,
			contact_phone: contact_phone,
			sph_video_feed_token: sph_video_feed_token,
			sph_image_urls: sph_image_urls,
			useds: selectUseds,
			bureaus: selectBureaus,
			is_anonymous: is_anonymous,
			voter_id: selectVoter
        },
        'POST'
    ).then(function (res) {
        uni.loading(false);
        if (res.code == 200) {
			
			that.$store.commit('Ipushlished', true);
			
			if(refer_id && refer_id > 0){
				that.openReferPopup(false);
				uni.showToast({
					title: '转发成功',
					icon: 'none'
				});
				return false;
			}else{
				uni.removeStorage({
					key: 'releaseDraft'
				});
				that.draftShow = false;
				
				uni.removeStorage({
					key: 'userSelectedTags'
				})
				uni.removeStorage({
					key: 'postsGoods'
				})
				uni.removeStorage({
					key: 'userSelectedCircle'
				})
				
				if(res?.data?.ok == 1){
					that.resData = res.data;
					// #ifdef MP-WEIXIN
					if(res.data.tmplIds && res.data.tmplIds.length > 0){
						wx.requestSubscribeMessage({
							tmplIds: res.data.tmplIds,
							success (res) {
								
							},
							fail(err) {
								console.log(err);
							}
						});
						return false;
					}
					// #endif
				}else{
					//  模板消息
					// #ifdef MP-WEIXIN
					if(res.data.tmplIds && res.data.tmplIds.length > 0){
						wx.requestSubscribeMessage({
							tmplIds: res.data.tmplIds,
							success (res) {
								setTimeout(function(){
									if(that?.editId > 0){
										uni.wen.toUrl(-6, '/pages/sticky/sticky?id=' + that?.editId, that);
									}else{
										uni.wen.toUrl(-2, 0, that);
									}
								}, 300);
							},
							fail(err) {
								uni.showToast({
									title: '已拒绝接受订阅消息',
									icon: 'none'
								});
								setTimeout(function(){
									if(that?.editId > 0){
										uni.wen.toUrl(-6, '/pages/sticky/sticky?id=' + that?.editId, that);
									}else{
										uni.wen.toUrl(-2, 0, that);
									}
								}, 300);
							}
						});
						return false;
					}
					// #endif
					uni.showToast({
						title: res.data.tip,
						icon: 'none'
					});
					setTimeout(function(){
						if(that?.editId > 0){
							uni.wen.toUrl(-6, '/pages/sticky/sticky?id=' + that?.editId, that);
						}else{
							uni.wen.toUrl(-2, 0, that);
						}
					}, 1500);
				}
			}
        } else if(res.code == 200016) {
            uni.showModal({
                title: res.data.tip[0] ? res.data.tip[0] : '存在违禁词',
                content: res.data.hit_word ? res.data.hit_word.join(' ') : '请勿发布违规内容',
                showCancel: false,
                confirmText: '朕知道了',
                confirmColor: that.primaryColor
            });
			return false;
        }else if( res.code == 200041 ){
			uni.showModal({
			    title: '默认昵称不能发布内容',
			    content: '为了您在社区的信誉，请勿以"微信用户","普通用户"'+ (that.$store.state.config.user?.default_user_name.length > 0 ? ',"'+that.$store.state.config.user?.default_user_name+'"' : '') +'开头',
			    showCancel: true,
			    confirmText: '修改昵称',
			    confirmColor: that.primaryColor,

			    success(res) {
			        if (res.confirm){
						that.draftShow = false;
						that.saveDraft();
						uni.wen.toUrl(6, '/pagesA/mine/editmine/editmine?name=1', that);
					}
			    }
			});
			return false;
		}else if(res.code == 200011){
				// #ifdef H5
				uni.showToast({
					title: uni.$store.state.config.app.coins.name + '不足！',
					icon: 'none'
				});
				return;
				// #endif
				if(that.$store.state.config.pays.paycode_14){
					let every_coin = 0;
					if(that.$store.state.config.app.coins && that.$store.state.config.app.coins.reward){
						// #ifdef MP
						every_coin = that.$store.state.config.app.coins.reward.mp.every;
						// #endif
						// #ifndef MP
						every_coin = that.$store.state.config.app.coins.reward.uni.every;
						// #endif
					}
					if(every_coin && every_coin > 0){
						uni.showModal({
							title:  uni.$store.state.config.app.coins.name + '不足！',
							content: '观看激励视频可获得' + every_coin + uni.$store.state.config.app.coins.name + '/次，是否观看？',
							confirmText: "获取",
							cancelText: "算了",
							success: function(res) {
								if (res.confirm) {
									uni.wen.toUrl(6, '/pagesA/mine/earnings/inspire/inspire', that);
								} else if (res.cancel) {

								}
							}
						});
					}else{
						uni.showToast({
							title: uni.$store.state.config.app.coins.name + '不足！',
							icon: 'none'
						});
					}
				}else{
					uni.showToast({
						title: uni.$store.state.config.app.coins.name + '不足！',
						icon: 'none'
					});
				}
			}else if(res.code == 200012){
				uni.showToast({
					title: res.message,
					icon: 'none',
					duration: 1500
				});
				that.draftShow = false;
				that.saveDraft();
				setTimeout(function(){
					uni.wen.toUrl(6, "/pagesA/mine/earnings/recharge/recharge", that);
				}, 1000);

			}else {
				if(res.message && res.message.length > 0){
					uni.showModal({
						title: '发布失败',
						content: res.message,
						showCancel: false,
						confirmText: '朕知道了',
						confirmColor: that.primaryColor
					});
				}
        }
    });
};

module.exports = function (obj) {
    obj.creatForums = creatForums;
};
