const postFeedback = function(obj) {
	let that = this;
	if(obj.action == 'sticky'){
		that.editOrStickyTap(obj.posts_id, obj.scene);
	}else if(obj.action == 'slider'){
		uni.wen.toUrl(obj.target_type, obj.target_id, null);
	}else if(obj.action == 'move'){
		uni.wen.toUrl(6, '/pages/circleClass/circleClass?post_id=' + obj.posts_id, null);
	}else if(obj.action == 'examine'){
		that.editOrExamineTap(obj.posts_id);
	}else if(obj.action == 'blur'){
		that.editOrBlurTap(obj.posts_id);
	}else if(obj.action == 'delete'){
		that.editOrDeleteTap(obj.posts_id);
	}else if(obj.action == 'report-0'){
		uni.loading(true);
		uni.wen.util.request(
			uni.wen.api.ApiRootUrl + 'user/report', {
				type: 0,
				object_id: obj.posts_id,
				report_type: obj.report_type,
				report_content: '',
				image_urls: [],
				contact: ''
			},
			'POST'
		).then(function(res) {
			uni.loading(false);
			if(res.status){
				uni.showToast({
					title: '举报成功！感谢您的监督',
					icon: 'none'
				});
			}else{
				uni.showToast({
					title: res.message,
					icon: 'none'
				});
			}
		});
	}else if(obj.action == 'feedback-notlike-1' || obj.action == 'feedback-notlike-2'){
		let type = 1;
		if(obj.action == 'feedback-notlike-2'){
			type = 2;
		}
		uni.wen.util.request(uni.wen.api.ApiRootUrl + 'posts/feedback', {
			posts_id: obj.posts_id,
			type: type
		}, 'POST').then(function(res) {
			uni.loading(false);
			if(res.status){
				uni.showToast({
					title: res.message,
					icon: 'none'
				});
				// 不喜欢该笔记
				if(type == 1){
					if(res.code == 200010){
						uni.showToast({
							title: res.message,
							icon: 'none'
						})
					}else{
						let that_ = uni.wen.util.getCurrentVm();
						if(that_ && that_.posts){
							for ( let i = 0; i < that_.posts.length; i ++ ) {
								if(that_.posts[i].id == obj.posts_id ){
									that_.posts.splice(i, 1);
									break;
								}
							}
							
							if(that_?.$refs?.productwater?.delete){
								that_.$refs.productwater.delete(obj.posts_id);
							}
						}
					}
				}else if(type == 2){
					if(res.code == 200010){
						uni.showToast({
							title: res.message,
							icon: 'none'
						})
					}else{
						// 不喜欢该用户
						let that_ = uni.wen.util.getCurrentVm();
						if(that_ && that_.posts){
							let indexesToRemove = [];
							for ( let i = 0; i < that_.posts.length; i ++ ) {
								if(that_.posts[i].user_id == res.data.post_user_id ){
									indexesToRemove.push(i);
								}
							}
							let newPosts = that_.posts.filter((element, index) => {
							  return !indexesToRemove.includes(index);
							});
							that_.posts = newPosts;
							
							if(that_?.$refs?.productwater?.delete_from_auth){
								that_.$refs.productwater.delete_from_auth(res.data.post_user_id);
							}
							
						}
					}
				}
				if(that.$store.state.scene == 9 && that.posts[0].id == obj.posts_id){
					uni.wen.toUrl(-2, '');
				}
			}else{
				uni.showToast({
					title: res.message,
					icon: 'none'
				})
			}

		});
	}else if(obj.action == 'limit'){
		that.limitTap(obj.posts_id);
	}
}

const limitTap = function(postsId){
	let that = this;
	uni.$store.commit('popopChange', false);
	
	uni.showActionSheet({
		itemList: ['1天', '3天', '6天', '10天', '30天', '100天'],
		success: function (res) {
			let limitDays = 1;
			if(res.tapIndex == 1){
				limitDays = 3;
			}else if(res.tapIndex == 2){
				limitDays = 6;
			}else if(res.tapIndex == 3){
				limitDays = 10;
			}else if(res.tapIndex == 4){
				limitDays = 30;
			}else if(res.tapIndex == 5){
				limitDays = 100;
			}
			uni.wen.util.request(
				uni.wen.api.ApiRootUrl + 'posts/process', {
					type: 'limit',
					posts_id: postsId,
					days: limitDays
				},
				'POST'
			).then(function(res) {
				uni.showToast({
					title: res.message,
					icon: 'none',
					duration: 1500
				});
				
			});
		},
		fail: function (res) {
			console.log(res.errMsg);
		}
	});
}


const editOrBlurTap = function(postsId){
	let that = this;
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'posts/process', {
			type: 'blur',
			posts_id: postsId
		},
		'POST'
	).then(function(res) {
		if (res.status) {

			let that_ = uni.wen.util.getCurrentVm();
			if(that_ && that_.posts){
				for ( let i = 0; i < that_.posts.length; i ++ ) {
					if(that_.posts[i].id == postsId ){
						that_.posts[i].is_blur = !that_.posts[i].is_blur;
						break;
					}
				}
			}

			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});

		}else{
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});
		}
		uni.$store.commit('popopChange', false);
	});
}



const editOrExamineTap = function(postsId){
	let that = this;
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'posts/process', {
			type: 'examine',
			posts_id: postsId
		},
		'POST'
	).then(function(res) {
		if (res.status) {

			let that_ = uni.wen.util.getCurrentVm();
			if(that_ && that_.posts){
				for ( let i = 0; i < that_.posts.length; i ++ ) {
					if(that_.posts[i].id == postsId ){
						if(that_.posts[i].is_examine == 1){
							that_.posts[i].is_examine = 2;
						}else{
							that_.posts[i].is_examine = 1;
						}
						break;
					}
				}
			}
			
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});

		}else{
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});
		}
		uni.$store.commit('popopChange', false);
	});
}

//显示删除菜单
const editOrDeleteTap = function(postsId) {
	let that = this;
	uni.showModal({
		title: '确认删除？',
		content: '请谨慎操作',
		showCancel: true,
		confirmText: '确定',
		confirmColor: that.primaryColor,
		success(res) {
			if (res.confirm){
				that.bouncedDeleteTap(postsId);
			}
		}
	});
	
};

const editOrStickyTap = function(postsId, scene){
	let that = this;
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'posts/process', {
			type: 'sticky',
			scene: scene,
			posts_id: postsId
		},
		'POST'
	).then(function(res) {
		if (res.status) {
			
			let that_ = uni.wen.util.getCurrentVm();
			if(that_ && that_.posts){
				for ( let i = 0; i < that_.posts.length; i ++ ) {
					if(that_.posts[i].id == postsId ){
						that_.posts[i].is_sticky = !that_.posts[i].is_sticky;
						break;
					}
				}
			}
			
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});

		}else{
			uni.showToast({
				title: res.message,
				icon: 'none',
				duration: 1500
			});
		}
		uni.$store.commit('popopChange', false);
	});
};

// 删除笔记接口
const bouncedDeleteTap = function(postsId) {
	let that = this;
	uni.wen.util.request(
		uni.wen.api.ApiRootUrl + 'posts/process', {
			type: 'delete',
			posts_id: postsId
		},
		'POST'
	).then(function(res) {
		if (res.status) {
			
			let that_ = uni.wen.util.getCurrentVm();
			if(that_ && that_.posts){
				for ( let i = 0; i < that_.posts.length; i ++ ) {
					if(that_.posts[i].id == postsId ){
						that_.posts.splice(i, 1);
						break;
					}
				}
				
				if( that_?.$refs?.productwater?.delete ){
					that_.$refs.productwater.delete(postsId);
				}
			}


			uni.showToast({
				title: res.message,
				icon: 'none'
			})

			setTimeout(function(){
				if(that.$store.state.scene == 9){
					uni.wen.toUrl(-2, 0, null);
				}
			}, 300);

		}else{
			uni.showToast({
				title: res.message,
				icon: 'none'
			})
		}
		uni.$store.commit('popopChange', false);
	});
};


module.exports = function (obj) {
	obj.postFeedback = postFeedback;
	obj.editOrBlurTap = editOrBlurTap;
	obj.limitTap = limitTap;
	obj.editOrStickyTap = editOrStickyTap;
	obj.editOrExamineTap = editOrExamineTap;
	obj.editOrDeleteTap = editOrDeleteTap;
	obj.bouncedDeleteTap = bouncedDeleteTap;
};