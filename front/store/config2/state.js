export default {
	"OK": false,
	"version": 1,
	"page": {
		"sticky": {
			"statement": "",
			"slider_comment_page": -1,
			"is_recommendation": 0
		},
		"circle": {
			"publish_icon": 0
		},
		"wallpapers": {
			"name": ""
		},
		"mine": {
			"about": {
				"list": []
			},
			"earnings": {
				"recharge_list": [],
				"balance_clause": 0,
				"child_refund_clause": 0,
				"withdrawal_way": [],
				"withdrawal_min_once": 1,
				"withdrawal_max_once": 200,
				"withdrawal_day_times": 3,
				"withdrawal_tips": []
			}
		},
		"publish2": {
			'bg': '',
			'items': [

			],
			'post': {
				'user': {}
			}
		},
		"used": {
			"index": {
				"header": {
				    "left": {
				    },
				    "right": {
				    }
				},
				"banner": []
			},
			"detail": {
				"map": 1,
				"buy": 1
			},
			"push": {
				"force_classify": 0
			}
		},
		"bureau": {
			"header": {
			    "left": {
			    },
			    "right": {
			    }
			},
			"index": {
				"banner": []
			},
			"settle_in": {
				"bg": ""
			},
			"detail": {
				"price": '',
				'price_gray': ''
			}
		},
		"reward": {
			"inspire": {
				"img": ""
			}
		},
		"certification": {
			"img_tip": []
		},
		"find": {
			"icons": {
				"header": [],
				"icons": []
			}
		},
		"voter": {
			"header": {
				"left": {
				},
				"right": {
				}
			},
			"banner": [],
			"tabs": []
		}
	},
	"emoji": {
		"page_1": [],
		"page_2": [],
		"page_3": [],
	},
	"invite": {
		"status": false,
		"lv": 2,
		"max_layer": 3
	},
	"app": {
		"withdrawal": {
			"platform_percent": 10
		},
		"contact_apply": {
			"platform_percent": 0
		}
	},
	"placeholder": {
		"publish_word": ""
	},
	"user": {
		"labels": []
	},
	"used": {
		"share": {
			"index_title": "",
			"index_image": "",
			"detail_title": ""
		}
	},
	"waterfall": {
		"scene_1": false,
		"scene_6": false,
		"scene_7": false,
		"scene_8": false,
	},
	"cos": {
		"bucket": "",
		"region": ""
	},
	"bureau": {
		"tags": [],
		"club": {
			"avatar": "",
			"clause": 0
		},
		"share": {
			"index_title": "",
			"index_image": "",
			"detail_title": ""
		}
	}
};