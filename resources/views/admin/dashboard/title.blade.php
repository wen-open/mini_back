<style>
    .dashboard-title .links {
        text-align: center;
        margin-bottom: 2.5rem;
    }
    .dashboard-title .links > a {
        padding: 0 25px;
        font-size: 12px;
        font-weight: 600;
        letter-spacing: .1rem;
        text-decoration: none;
        text-transform: uppercase;
        color: #fff;
    }
    .dashboard-title h1 {
        font-weight: 200;
        font-size: 2.5rem;
    }
    .dashboard-title .avatar {
        background: #fff;
        border: 2px solid #fff;
        width: 70px;
        height: 70px;
    }
</style>

<div class="dashboard-title card bg-primary">
    <div class="card-body">
        <div class="text-center ">

            <img class="avatar img-circle shadow mt-1" src="{{$logo}}">

            <div class="text-center mb-1">
                <h1 class="mb-3 mt-2 text-white">
                    {{$name}}
                    <span style="font-size: 16px; margin-left: 10px;">v{{ $version  }}</span>
                    @if($version_tip)
                        <a href="{{ $version_bt_url }}" target="_blank" style="cursor: pointer;font-size: 14px; margin-left: 10px;background-color: black;color: #ffffff; border-radius: 30px; padding: 0px 10px;">v{{ $version_tip  }}</a>
                    @endif
                </h1>
                <div class="links">
                    <a href="{{ $site }}" target="_blank">官网</a>
                    <a href="{{ $agent }}" id="doc-link" target="_blank">代理商</a>
                    <a href="{{ $customized }}" id="demo-link" target="_blank">定制服务</a>
                    <a href="{{$doc}}" id="demo-link" target="_blank">教程文档</a>
                </div>
            </div>
        </div>
    </div>
</div>
