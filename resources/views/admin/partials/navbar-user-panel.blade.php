<?php ?>
@if($user)
<li class="dropdown dropdown-user nav-item">
    <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
        <div class="user-nav d-sm-flex d-none">
            <span class="user-name text-bold-600">{{ $user->name }}</span>
            <span class="user-status"><i class="fa fa-circle text-success"></i> {{ trans('admin.online') }}</span>
        </div>
        <span>
            <img class="round" src="{{ $user->getAvatar() }}" alt="avatar" height="40" width="40" />
        </span>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <a href="{{ admin_url('auth/setting') }}" class="dropdown-item">
            <i class="feather icon-user"></i> {{ trans('admin.setting') }}
        </a>

        <div class="dropdown-divider"></div>

        @if($is_super)
        <a href="{{ admin_url('settings/front') }}" class="dropdown-item">
            <i class="fa fa-flag-checkered"></i> {{ trans('admin.down_front') }}
        </a>
        <div class="dropdown-divider"></div>
        <a href="{{ admin_url('/tenants') }}" class="dropdown-item">
            <i class="fa fa-clone"></i> {{ trans('admin.tenants') }}
        </a>
        <div class="dropdown-divider"></div>
        <a href="{{ admin_url('/domain/auth') }}" class="dropdown-item">
            <i class="fa fa-creative-commons"></i> {{ trans('admin.domain_auth') }}
        </a>
        <div class="dropdown-divider"></div>
        <a href="{{ admin_url('/admin/tools') }}" class="dropdown-item">
            <i class="fa fa-wrench"></i> {{ trans('admin.toolbox') }}
        </a>
        <div class="dropdown-divider"></div>
        @endif

        <a class="dropdown-item" href="{{ admin_url('auth/logout') }}">
            <i class="feather icon-power"></i> {{ trans('admin.logout') }}
        </a>
    </div>
</li>
@endif
