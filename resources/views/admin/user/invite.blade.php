
<style>
.invite-users{
    display: flex;
    flex-wrap: wrap;
    max-height: 80vh;
    overflow-y: scroll;
}
.invite-users .item{
    display: flex;
    margin-right: 20px;
    margin-bottom: 30px;
}
.invite-users .avatar{
    width: 30px;height: 30px;border-radius: 50%;
    margin-right: 6px;
}
.invite-users .name{
    line-height: 30px;
}
</style>


<div class="invite-users">
    @foreach($user_list as $user)
        <a class="item" target="_blank" href="{{ admin_url('users?id='.$user['id']) }}" title="{{ $user['id'] }}">
            <img class="avatar" src="{{  $user['user_avatar'] }}">
            <span class="name">{{ $user['user_name'] }}</span>
        </a>
    @endforeach
</div>

