<style>
    .info—box{
        display: flex;
    }
    .info—box-line{
        height: 1px;
        width: 100%;
        background-color: lightgrey;
        margin-bottom: 10px;
    }
</style>
<div>

    <div class="info—box">
        <h5>昵称：</h5>
        <p>{{$user['user_name']}}</p>
    </div>
    <div class="info—box">
        <h5>背景：</h5>
        <p><img src="{{$user['user_background_maps']}}" alt="" width="159"></p>
    </div>
    <div class="info—box">
        <h5>简介：</h5>
        <p>{{$user['user_introduce']}}</p>
    </div>
    <div class="info—box">
        <h5>手机：</h5>
        <p>{{$user['origin_phone']}}</p>
    </div>
    <div class="info—box">
        <h5>邮箱：</h5>
        <p>{{$user['email']}}</p>
    </div>
    <div class="info—box">
        <h5>拉黑原因：</h5>
        <p>{{$user['black_reason']}}</p>
    </div>
    <div class="info—box">
        <h5>Ip地址：</h5>
        <p>{{$user['ip']}}</p>
    </div>
    <div class="info—box">
        <h5>Ip属地：</h5>
        <p>{{$user['country'] .'-'. $user['province'] .'-'. $user['city'] .'-'. $user['district']}}</p>
    </div>
    <div class="info—box-line"></div>
    <div class="info—box">
        <h5>设备型号：</h5>
        <p>{{isset($user['online']) ? ($user['online']['device_model'] ?? '') : ''}}</p>
    </div>
    <div class="info—box">
        <h5>操作系统名称及版本：</h5>
        <p>{{isset($user['online']) ? ($user['online']['system'] ?? '') : ''}}</p>
    </div>
    <div class="info—box">
        <h5>Mac地址：</h5>
        <p>{{isset($user['online']) ? ($user['online']['wifi_mac_address'] ?? '') : ''}}</p>
    </div>
</div>

