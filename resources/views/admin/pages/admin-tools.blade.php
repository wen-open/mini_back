<?php
?>
<div class="admin-tools" id="admin-tools">
    <div class="tool-card" id="img-domain-replace">
        <div class="icon">
            <i class="feather icon-repeat"></i>
        </div>
        <strong> 数据库字符替换
        </strong>
        <div class="card__body">
            全局替换所有表的所有字段...
        </div>
        <span class="desc">全局替换所有表的所有字段a到b，但在这之前，请提前备份，后果自负</span>
    </div>
    <div class="tool-card" id="chat-record-change">
        <div class="icon">
            <i class="feather icon-crop"></i>
        </div>
        <strong> 聊天记录迁移
        </strong>
        <div class="card__body">
            将某个账号的聊天记录全部迁移到另外一个账号...
        </div>
        <span class="desc">比如你以前把a账号设置为了小队长，现在想将b账号设置为小队长，则可以先把a账号的聊天记录全部迁移到b账号</span>
    </div>
</div>

<script init=".admin-tools">

</script>

<style>
    .admin-tools {
        background-color: #fff;
        min-height: 85vh;
        width: 100%;
        padding: 10px;
    }




    .tool-card {
        display: inline-block;
        height: 170px;
        --bg: #f7f7f8;
        --hover-bg: #f7f7f8;
        --hover-text: #333333;
        max-width: 23ch;
        text-align: center;
        background: var(--bg);
        padding: 1.5em;
        padding-block: 1.8em;
        border-radius: 5px;
        position: relative;
        overflow: hidden;
        transition: .3s cubic-bezier(.6,.4,0,1),transform .15s ease;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        gap: 1em;
        cursor: pointer;
        margin-right: 10px;
    }

    .tool-card .icon .feather{
        font-size: 26px;
        color: #000000;
    }

    .card__body {
        color: #464853;
        line-height: 1.5em;
        font-size: 1em;
    }

    .tool-card > :not(span) {
        transition: .3s cubic-bezier(.6,.4,0,1);
    }

    .tool-card > strong {
        display: block;
        font-size: 1.4rem;
        letter-spacing: -.035em;
    }

    .tool-card span {
        position: absolute;
        inset: 0;
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        color: var(--hover-text);
        border-radius: 5px;
        font-weight: bold;
        top: 100%;
        transition: all .3s cubic-bezier(.6,.4,0,1);
    }

    .tool-card:hover span {
        top: 0;
        font-size: 1.2em;
    }

    .tool-card:hover span.desc{
        transform: scale(0.8);
    }

    .tool-card:hover {
        background: var(--hover-bg);
    }

    .tool-card:hover>div,.tool-card:hover>strong {
        opacity: 0;
    }
</style>
