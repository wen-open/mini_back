
<?php
?>

<form class="input-no-border quick-search-form" id="search-domain-auth" style="margin-right: 16px; margin-bottom: 20px;display: none;">
    <div class="table-filter">
        <label style="width: 50rem">
            <input id="domain-auth-search" type="search" class="form-control form-control-sm" placeholder="搜索Ta的授权，仅输入域名即可" name="domain" value="{{ $current_domain }}">
        </label>
    </div>
</form>

<div class="appstore-mask" id="appstore-mask" style="display: none" onclick="appstore_modal_click(-1, false)"></div>

<div class="app-center" id="app-center">
    <div class="appstore-main-wrapper" id="appstore-main-wrapper" style="display: none">
        <div class="inner">
            <div class="appstore-header" style="display: inline-flex; float: right; position: absolute; right: 15px; top: 10px;">
                <div onclick="appstore_download_licene()" style="color: red;cursor: pointer;" id="down-licene">下载我的授权文件</div>
                <div style="width: 4px;"></div>
                <a href="https://doc.minisns.cn/doc/140/" target="_blank" title="点击查看教程"><i class="fa feather icon-help-circle"></i></a>
                <div style="width: 20px;"></div>
                <div onclick="appstore_login(2)" style="cursor: pointer;">登出</div>
            </div>
            <div class="appstore-main-title title-1">我的模块</div>
            <div class="appstore-main-modules-list">
                <div class="appstore-model-item index_0 {{ $paycode_0 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(0, true)" goodsid="2899">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/06/07/0/65d91b424f0b72cb761857ef016cff19.png">
                    <div class="appstore-model-name">付费0：基础授权</div>
                </div>
                <div class="appstore-model-item index_1 {{ $paycode_1 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(1, true)" goodsid="2473">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/3b82c88491c4e2cdd33e6b95b6818e41.png">
                    <div class="appstore-model-name">付费1：搜索高亮</div>
                </div>
                <div class="appstore-model-item index_2 {{ $paycode_2 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(2, true)" goodsid="2474">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/0451c49568eab3a7a76d43f07e61cc73.png">
                    <div class="appstore-model-name">付费2：服务号模板消息+小程序订阅消息</div>
                </div>
                <div class="appstore-model-item index_3 {{ $paycode_3 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(3, true)" goodsid="2477">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/55535a27e81ec6a3680199bfe8b7fab4.png">
                    <div class="appstore-model-name">付费3：附件自动清理</div>
                </div>
                <div class="appstore-model-item index_4 {{ $paycode_4 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(4, true)" goodsid="2475">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/88fdda8f157ecd6e6490cc83762300aa.png">
                    <div class="appstore-model-name">付费4：文字内容审核</div>
                </div>
                <div class="appstore-model-item index_5 {{ $paycode_5 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(5, true)" goodsid="2472">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/04/b1d42c5018bf0fd2931e4c6a3e61f22a.webp" style="transform: scale(0.4);">
                    <div class="appstore-model-name">付费5：首页内容弱算法排序</div>
                </div>
                <div class="appstore-model-item index_6 {{ $paycode_6 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(6, true)" goodsid="2476">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/f6dbc9c04cca786f81f7bbda4f7e1c85.png">
                    <div class="appstore-model-name">付费6：websocket在线即时通知</div>
                </div>
                <div class="appstore-model-item index_7 {{ $paycode_7 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(7, true)" goodsid="2478">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/6f3cf88280fd2b80528c89cfed63fded.png">
                    <div class="appstore-model-name">付费7：余额付费贴，附件付费贴</div>
                </div>
                <div class="appstore-model-item index_8 {{ $paycode_8 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(8, true)" goodsid="2479">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/04/ffebad5732ecb6740c541b019c85d3f0.png">
                    <div class="appstore-model-name">付费8：页面模板</div>
                </div>
                <div class="appstore-model-item index_9 {{ $paycode_9 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(9, true)" goodsid="2480">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/04/b9e86d575ada96f0d3b11c6aa9f9e0c4.png">
                    <div class="appstore-model-name">付费9：活跃系统</div>
                </div>
                <div class="appstore-model-item index_10 {{ $paycode_10 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(10, true)" goodsid="2481">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/02162ced5ee5c37ada482e1602557a9a.png">
                    <div class="appstore-model-name">付费10：seo额外支持</div>
                </div>
                <div class="appstore-model-item index_11 {{ $paycode_11 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(11, true)" goodsid="2482">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/04/0e198df430b6c9d3ec67960e51da1a40.png">
                    <div class="appstore-model-name">付费11：( 核销，卡密，虚拟) 商品</div>
                </div>
                <div class="appstore-model-item index_12 {{ $paycode_12 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(12, true)" goodsid="2483">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/04/ff1ecc3b9fb4ff4b9a3df444ba1c5dc0.png">
                    <div class="appstore-model-name">付费12：实时热榜</div>
                </div>
                <div class="appstore-model-item index_13 {{ $paycode_13 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(13, true)" goodsid="2733">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/09/04/05941e8865e18d64e8ae562d12c65cfe.jpg">
                    <div class="appstore-model-name">付费13：淘客</div>
                </div>
                <div class="appstore-model-item index_14 {{ $paycode_14 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(14, true)" goodsid="2484">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/05/f2caf6b46a127f7dc6a826c43a262d65.png">
                    <div class="appstore-model-name">付费14：广告</div>
                </div>
                <div class="appstore-model-item index_15 {{ $paycode_15 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(15, true)" goodsid="2794">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/11/10/a6d895c273cab79783db04f3b7574910.png">
                    <div class="appstore-model-name">付费15：圈子扩展</div>
                </div>
                <div class="appstore-model-item index_16 {{ $paycode_16 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(16, true)" goodsid="2680">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/06/02/369d0c3ec94e047241125f39334fbfaa.png">
                    <div class="appstore-model-name">付费16：迷你会员</div>
                </div>
                <div class="appstore-model-item index_17 {{ $paycode_17 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(17, true)" goodsid="2492">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/06/0fb90914a77ac55d588a06fdb7c039fb.png">
                    <div class="appstore-model-name">付费17：金币抵扣组合支付</div>
                </div>
                <div class="appstore-model-item index_18 {{ $paycode_18 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(18, true)" goodsid="2551">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/14/c3218c08fd0d4b3c5928d84fffec20fb.png">
                    <div class="appstore-model-name">付费18：语音功能</div>
                </div>
                <div class="appstore-model-item index_19 {{ $paycode_19 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(19, true)" goodsid="2573">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/20/b204e5cde1cf41a2849cbad8e7f22323.png">
                    <div class="appstore-model-name">付费19：App窗口进出动画自定义</div>
                </div>
                <div class="appstore-model-item index_20 {{ $paycode_20 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(20, true)" goodsid="2669">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/05/28/98ca91c5549e76fa2528e3ebe02e520c.png">
                    <div class="appstore-model-name">付费20：团队推广三级裂变</div>
                </div>
                <div class="appstore-model-item index_21 {{ $paycode_21 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(21, true)" goodsid="2715">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/07/29/cc46c4e857f7b416d5813ba4594a0720.png">
                    <div class="appstore-model-name">付费21：站内转发+转发好友</div>
                </div>
                <div class="appstore-model-item index_22 {{ $paycode_22 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(22, true)" goodsid="2719">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/08/05/b6f530ac0ffae497ae6db907ae72dfad.png">
                    <div class="appstore-model-name">付费22：暗黑模式</div>
                </div>
                <div class="appstore-model-item index_23 {{ $paycode_23 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(23, true)" goodsid="2722">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/08/11/26d88e56024706af29ad3b794f49812a.png">
                    <div class="appstore-model-name">付费23：商品用户分账</div>
                </div>
                <div class="appstore-model-item index_24 {{ $paycode_24 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(24, true)" goodsid="2729">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/08/27/9d3b6e905aec6e1fa1455cfe3eaf0e15.jpg">
                    <div class="appstore-model-name">付费24：上下滑动视频</div>
                </div>
                <div class="appstore-model-item index_25 {{ $paycode_25 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(25, true)" goodsid="2776">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/09/29/e2e8fe7a81ca1a22e9802e9006f56fbe.png">
                    <div class="appstore-model-name">付费25：宠物领养系统</div>
                </div>
                <div class="appstore-model-item index_26 {{ $paycode_26 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(26, true)" goodsid="2740">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/09/05/f582aa01e444e2b5fe434a9dc8f31f08.jpg">
                    <div class="appstore-model-name">付费26：抽奖获得商品</div>
                </div>
                <div class="appstore-model-item index_27 {{ $paycode_27 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(27, true)" goodsid="2748">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/09/09/4997641b1541cb25c5d74f27e80c21ba.png">
                    <div class="appstore-model-name">付费27：发布视频号视频+直播</div>
                </div>
                <div class="appstore-model-item index_28 {{ $paycode_28 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(28, true)" goodsid="2797">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/11/17/f941b5e3845dd785921edd4ac54e8a79.jpg">
                    <div class="appstore-model-name">付费28：DESAI PC端</div>
                </div>
                <div class="appstore-model-item index_29 {{ $paycode_29 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(29, true)" goodsid="2805">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2023/12/01/fc5f25ee1758a1562ed4a682110ac42b.png">
                    <div class="appstore-model-name">付费29：Mini收集器</div>
                </div>
                <div class="appstore-model-item index_30 {{ $paycode_30 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(30, true)" goodsid="2817">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/01/22/74c27ce83428612cd030ec9dbf60445c.png">
                    <div class="appstore-model-name">付费30：二手系统</div>
                </div>
                <div class="appstore-model-item index_31 {{ $paycode_31 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(31, true)" goodsid="2824">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/03/02/0/93b36bfc492c6dfab6a7d651f54a4005.png">
                    <div class="appstore-model-name">付费31：多商户</div>
                </div>
                <div class="appstore-model-item index_32 {{ $paycode_32 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(32, true)" goodsid="2886">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/04/05/0/cf588ee31c103ccf3af9516a0f563636.png">
                    <div class="appstore-model-name">付费32：群推送</div>
                </div>
                <div class="appstore-model-item index_33 {{ $paycode_33 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(33, true)" goodsid="2895">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/04/21/0/000b7bbd285d6ce7ddbb7dfaf7bcd517.png">
                    <div class="appstore-model-name">付费33：选票活动</div>
                </div>
                <div class="appstore-model-item index_34 {{ $paycode_34 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(34, true)" goodsid="2897">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/05/28/0/0829cd36e5d54f66b84d72a0e8c40b6b.png">
                    <div class="appstore-model-name">付费34：公众号自动推文</div>
                </div>
                <div class="appstore-model-item index_35 {{ $paycode_35 ? '' : ' appstore-gray-filter' }}">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/07/05/0/0cbbbae7ea5d37afef49dca5ab1b5b67.png">
                    <div class="appstore-model-name">付费35：组局活动</div>
                </div>
                <div class="appstore-model-item index_36 {{ $paycode_36 ? '' : ' appstore-gray-filter' }}" onclick="appstore_modal_click(34, true)" goodsid="2908">
                    <img class="appstore-model-img" src="https://img.mini.minisns.cn/2024/09/21/0/b853d4dad2a1fe4a23f31571bb53749a.png">
                    <div class="appstore-model-name">付费36：付费获取联系方式</div>
                </div>

            </div>
            <div style="height: 30px;"></div>
            <div class="appstore-main-title title-2">我的分站</div>
            <div style="height: 30px;"></div>
            <div class="tenant_list_container" id="tenant-list-container"></div>
            <div style="height: 10px;"></div>
        </div>
    </div>

    <div class="appstore-login-wrapper" id="appstore-login-wrapper" style="display: none;">
        <div class="inner">
            <img class="code" id="appstore-login-scan-code">
            <div class="tip">微信扫码登录</div>
        </div>
    </div>

    <div class="appstore-model-buy-box" id="appstore-model-buy-box" style="display: none;">
        <div class="model-card">
            <div class="model-tip" style="display: none;"></div>
            <div class="model-glasses">
                <image class="code-img" id="model-code-img" src="/statics/images/loading.gif"></image>
            </div>
            <div class="model-heading"></div>
            <div class="model-details"></div>
            <div class="model-price"></div>
            <button class="model-btn model-btn0" style="display: none;" onclick="appstore_change_pay_type(0)">微信</button>
            <button class="model-btn model-btn1" onclick="appstore_change_pay_type(1)">支付宝</button>
            <button class="model-btn model-btn2" onclick="appstore_change_pay_type(2)">余额支付</button>
            <div class="model-res-balance">官网剩余余额：￥30.00</div>
        </div>
    </div>

    <!-- 自定义模态框 -->
    <div class="custom-modal" id="custom-modal" style="display: none;">
        <div class="custom-modal-content">
            <span class="close" onclick="closeCustomModal()">&times;</span>
            <div id="custom-modal-message"></div>
            <input type="text" id="custom-modal-input" style="display: none;" maxlength="6" pattern="\d{6}">
            <button onclick="handleCustomModalSubmit()">确定</button>
        </div>
    </div>


    <script init=".app-center">
        var scan_login_scene = '';
        var is_first_search = true;
        var paing_model_num = -1;
        var paycodes = [
            {{ $paycode_0 }}, {{ $paycode_1 }}, {{ $paycode_2 }},
            {{ $paycode_3 }}, {{ $paycode_4 }}, {{ $paycode_5 }},
            {{ $paycode_6 }}, {{ $paycode_7 }}, {{ $paycode_8 }},
            {{ $paycode_9 }}, {{ $paycode_10 }}, {{ $paycode_11 }},
            {{ $paycode_12 }}, {{ $paycode_13 }}, {{ $paycode_14 }},
            {{ $paycode_15 }}, {{ $paycode_16 }}, {{ $paycode_17 }},
            {{ $paycode_18 }}, {{ $paycode_19 }}, {{ $paycode_20 }},
            {{ $paycode_21 }}, {{ $paycode_22 }}, {{ $paycode_23 }},
            {{ $paycode_24 }}, {{ $paycode_25 }}, {{ $paycode_26 }},
            {{ $paycode_27 }}, {{ $paycode_28 }}, {{ $paycode_29 }},
            {{ $paycode_30 }}, {{ $paycode_31 }}, {{ $paycode_32 }},
            {{ $paycode_33 }}, {{ $paycode_34 }}, {{ $paycode_35 }}, {{ $paycode_36 }}
        ];
        var localDomain = "{{ get_mini_domain() }}";

        function appstore_post(url, data = {}, type = 'POST', headers = {}) {
            var base_headers = {
                'device': 'pc',
                'token': appstore_get_cookie('appstore-token')
            };
            return new Promise(function (resolve, reject) {
                $.ajax({
                    type: type,
                    dataType: "json",
                    url: 'https://mini.minisns.cn/api/v1/' + url,
                    data: data,
                    async: true,
                    headers: { ...headers, ...base_headers },
                    success: function (msg) {
                        resolve(msg);
                    },
                    error: function (err) {
                        reject(err);
                    }
                });
            });
        }

        async function appstore_is_uid_exists(user_id) {
            user_id = parseInt(user_id);
            var resp = {
                'code': 0,
                'msg': ''
            };
            if (!isNaN(user_id) && user_id > 0) {
                console.log('Checking UID:', user_id); // 调试信息
                try {
                    const res1 = await appstore_post('user/info/byUserId', { "user_id": user_id }, 'GET');
                    console.log('First API call response:', res1); // 调试信息
                    if (res1.status) {
                        const res2 = await appstore_post('aggregate/user/hanlder', { 'action': 'authorization', 'user_id': user_id });
                        console.log('Second API call response:', res2); // 调试信息
                        if (res2.status) {
                            resp['code'] = 200010;
                            resp['msg'] = 'Ta已经有授权了，无需再次购买';
                            throw resp;
                        } else {
                            resp['code'] = 200;
                            resp['uid'] = user_id;
                            return resp;
                        }
                    } else {
                        resp['code'] = res1.code;
                        resp['msg'] = res1.message;
                        throw resp;
                    }
                } catch (error) {
                    console.error('Error in appstore_post:', error);
                    throw { 'code': 500, 'msg': error.msg };
                }
            } else {
                resp['code'] = 200001;
                resp['msg'] = '无效输入';
                throw resp;
            }
        }

        async function appstore_modal_click(num, flag) {
            if (flag) {
                var userInput = null;
                var isEditUid = false;
                var search_domain = ($('#domain-auth-search').val() || '{{ $current_domain }}') || localDomain;
                if (search_domain !== localDomain && !is_first_search && $('#domain-auth-search').val() === '') {
                    search_domain = localDomain;
                }

                if (!$('.appstore-model-item.index_' + num).hasClass('appstore-gray-filter')) {
                    Dcat.warning('无需重复购买');
                    return false;
                }

                while (!($('#down-licene').attr('uid') > 0)) {
                    if (search_domain === localDomain) {
                        userInput = await openCustomModal(search_domain + "没有授权记录，请输入你在官网小程序的UID，才可以购买授权或模块", true);
                    } else {
                        userInput = await openCustomModal(search_domain + "没有授权记录，请输入他Ta在官网小程序的UID，才可以为Ta购买授权或模块", true);
                    }

                    if (userInput === null) {
                        return false; // 用户取消输入，返回
                    }

                    if (userInput !== '') {
                        try {
                            const check_res = await appstore_is_uid_exists(userInput);
                            $('#down-licene').attr('uid', check_res['uid']);
                            console.log('UID set to:', check_res['uid']); // 调试信息
                            isEditUid = true;
                            break; // 成功设置UID后跳出循环
                        } catch (check_res) {
                            Dcat.error(check_res['msg']);
                            console.log('UID check error:', check_res); // 调试信息
                        }
                    } else {
                        Dcat.error('无效输入，请重新输入');
                    }
                }

                if (!($('#down-licene').attr('uid') > 0)) {
                    return false; // 确保正确设置UID后继续执行后续代码
                }

                paing_model_num = num;

                $('#appstore-mask').show();
                $('#appstore-model-buy-box').show();

                if (search_domain !== localDomain) {
                    $('#appstore-model-buy-box .model-tip').text('您正在为' + search_domain + '购买模块').show();
                } else {
                    $('#appstore-model-buy-box .model-tip').text('').hide();
                }

                var goods_id = $('.appstore-model-item.index_' + num).attr('goodsid');
                if (!(goods_id > 0)) {
                    Dcat.error('获取模块信息失败');
                    return false;
                }

                try {
                    const res = await appstore_post('shop/goodsDetails', { "id": goods_id }, 'GET');
                    if (res.status) {
                        var price_tip = '￥' + res.data.price;
                        $('#appstore-model-buy-box .model-heading').text(res.data.name);
                        $('#appstore-model-buy-box .model-details').text(res.data.intro);
                        $('#appstore-model-buy-box .model-price').text(price_tip);
                        $('#appstore-model-buy-box .model-res-balance').text("官网剩余余额：￥" + $('#down-licene').attr('balance'));
                    } else {
                        Dcat.error(res.message);
                        return false;
                    }
                } catch (error) {
                    Dcat.error('获取模块信息失败');
                    return false;
                }

                appstore_change_pay_type(0);

            } else {
                paing_model_num = -1;
                $('#appstore-mask').hide();
                $('#appstore-model-buy-box').hide();
                appstore_check_auth();
                appstore_update_user_balance();
            }
        }
        async function appstore_change_pay_type(which) {
            var provider = '';
            var search_domain = ($('#domain-auth-search').val() || '{{ $current_domain }}') || localDomain;
            if (search_domain != localDomain && !is_first_search && $('#domain-auth-search').val() == '') {
                search_domain = localDomain;
            }

            if (!$('.appstore-model-item.index_' + paing_model_num).hasClass('appstore-gray-filter')) {
                Dcat.warning('无需重复购买');
                return false;
            }

            while (!(parseInt($('#down-licene').attr('uid')) > 0)) {
                try {
                    await appstore_modal_click(paing_model_num, true);
                } catch (e) {
                    return false; // 用户取消或出错后退出
                }
            }

            if (which === 0) {
                $('#appstore-model-buy-box .model-btn0').hide();
                $('#appstore-model-buy-box .model-btn1').show();
                $('#appstore-model-buy-box .model-btn2').show();
                provider = 'wxpay';
            } else if (which === 1) {
                $('#appstore-model-buy-box .model-btn0').show();
                $('#appstore-model-buy-box .model-btn1').hide();
                $('#appstore-model-buy-box .model-btn2').show();
                provider = 'alipay';
            } else if (which === 2) {
                $('#appstore-model-buy-box .model-btn0').show();
                $('#appstore-model-buy-box .model-btn1').show();
                $('#appstore-model-buy-box .model-btn2').hide();

                let paycode = null;
                while (paycode === null) {
                    paycode = await openCustomModal('输入支付密码', true);
                    if (paycode !== null) {
                        const regex = /^\d{6}$/;
                        if (regex.test(paycode)) {
                            var balanceForm = {
                                'paycode': paycode,
                                'order': {
                                    'type': 9,
                                    'num': paing_model_num,
                                    'domain': search_domain,
                                    'user_id': $('#down-licene').attr('uid') > 0 ? $('#down-licene').attr('uid') : 0,
                                    'coins_num': 0
                                }
                            };
                            try {
                                const res = await appstore_post('balance_pay', balanceForm);
                                if (res.status) {
                                    paycode = '';
                                    appstore_modal_click(0, false);
                                    appstore_check_auth();
                                    appstore_update_user_balance();
                                    return false;
                                } else {
                                    paycode = null;
                                    await openCustomModal(res.message);
                                }
                            } catch (error) {
                                Dcat.error('支付过程中出现错误');
                                paycode = null;
                            }
                        } else {
                            paycode = null;
                            await openCustomModal('无效输入，请输入6位数字的支付密码，若忘记密码，可以去官网重置');
                        }
                    } else {
                        paycode = '';
                        appstore_modal_click(0, false);
                    }
                }
                return false;
            }

            var orderForm = {
                'provider': provider,
                'pay_type': 'code',
                'type': 9,
                'parame': {
                    'num': paing_model_num,
                    'domain': search_domain,
                    'user_id': $('#down-licene').attr('uid') > 0 ? $('#down-licene').attr('uid') : 0
                }
            };

            try {
                const res = await appstore_post('app/order', orderForm);
                if (res.status) {
                    $('#model-code-img').attr('src', res.data.code);
                } else {
                    Dcat.error(res.message);
                    return false;
                }
            } catch (error) {
                Dcat.error('下单过程中出现错误');
            }
        }

        function appstore_set_cookie(name, value) {
            var Days = 14; // 30天
            var exp = new Date();
            exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
            document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
        }

        function appstore_get_cookie(name) {
            var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
            if (arr = document.cookie.match(reg)) {
                return unescape(arr[2]);
            } else {
                return null;
            }
        }

        async function appstore_check_auth() {
            var search_domain = ($('#domain-auth-search').val() || '{{ $current_domain }}') || localDomain;
            var is_need_filter = true;
            if (search_domain != localDomain && !is_first_search && $('#domain-auth-search').val() == '') {
                search_domain = localDomain;
            }
            $("#tenant-list-container").empty();
            if (search_domain != localDomain) {
                $('.appstore-model-item.dashed').removeClass('dashed');
                $('.appstore-model-item').addClass('appstore-gray-filter');
                $('.appstore-main-title.title-1').text('Ta的模块');
                $('.appstore-main-title.title-2').text('Ta的分站');
                $('#down-licene').text('下载Ta的授权文件').css('color', 'blue');
            } else if (search_domain == localDomain) {
                is_need_filter = false;
                $('.appstore-main-title.title-1').text('我的模块');
                $('.appstore-main-title.title-2').text('我的分站');
                $('#down-licene').text('下载我的授权文件').css('color', 'red');
            }
            is_first_search = false;
            try {
                var currentUrl = window.location.href;
                var newUrl = currentUrl.replace(/\?.*$/, '') + '?domain=' + search_domain;
                history.replaceState(null, '', newUrl);
            } catch (e) {
                console.error('History update failed:', e);
            }
            try {
                const res = await appstore_post('aggregate/user/hanlder', { 'action': 'authorization', 'domain': search_domain });
                if (res.status) {
                    const data = res.data;
                    let foundDomain = null;
                    let foundTenant = null;

                    $('#down-licene').attr('uid', data.user_id);

                    for (let i = 1; i <= 7; i++) {
                        const domainKey = `domain_${i}`;
                        const tenantKey = `tenant_${i}`;

                        if (data[domainKey] && data[domainKey] === search_domain) {
                            foundDomain = data[domainKey];
                            foundTenant = data[tenantKey];
                            break;
                        }
                    }

                    const result1 = {
                        match: foundDomain !== null,
                        domain: foundDomain,
                        tenant: foundTenant
                    };

                    if (result1.match) {

                        var paysArray = data.pays.split(',').map(Number);
                        var nonMatchingNumbers = [];

                        paysArray.forEach(function (number) {
                            if (Boolean(paycodes[number]) !== true) {
                                nonMatchingNumbers.push(number);
                            }

                            if (is_need_filter) {
                                if (search_domain == localDomain) {
                                    if (Boolean(paycodes[number])) {
                                        $('.appstore-model-item.index_' + number).removeClass('appstore-gray-filter');
                                    }
                                } else {
                                    $('.appstore-model-item.index_' + number).removeClass('appstore-gray-filter');
                                }
                            }

                        });

                        paycodes.forEach(function (value, index) {
                            if (Boolean(value) === true && !paysArray.includes(index)) {
                                nonMatchingNumbers.push(index);
                                if (search_domain == localDomain) {
                                    $('.appstore-model-item.index_' + index).removeClass('appstore-gray-filter');
                                }
                            }
                        });

                        if (search_domain == localDomain) {
                            if (nonMatchingNumbers.length > 0) {
                                nonMatchingNumbers.forEach(function (number) {
                                    $('.appstore-model-item.index_' + number).addClass('dashed').attr('title', '当前模块需要更新授权文件才能生效');
                                });
                                if (search_domain == localDomain) {
                                    Dcat.error('您需要更新授权文件');
                                }
                            } else {
                                $('.appstore-model-item.dashed').removeClass('dashed');
                            }
                        }

                        var tenantListContainer = $("#tenant-list-container");
                        if (result1.tenant) {
                            result1.tenant.split(',').forEach(function (tenant) {
                                var tenantCard = document.createElement('div');
                                tenantCard.className = 'tenant_card';
                                tenantCard.textContent = tenant;
                                tenantListContainer.append(tenantCard);
                            });
                        }
                    } else {
                        // 未匹配上
                        if (search_domain == localDomain) {
                            Dcat.error('经系统检测，您疑似登错官网账号');
                        }
                    }
                } else {
                    $('#down-licene').attr('uid', 0);
                    Dcat.error(res.message);
                }
            } catch (error) {
                console.error('Error in appstore_check_auth:', error);
            }
        }

        async function appstore_update_user_balance() {
            try {
                const res = await appstore_post('user/myFinancial', {}, 'GET');
                if (res.status) {
                    $('#down-licene').attr('balance', res.data?.financial?.balance ?? 0);
                } else {
                    Dcat.error(res.message);
                    return false;
                }
            } catch (error) {
                console.error('Error in appstore_update_user_balance:', error);
            }
        }

        function appstore_login_try() {
            var times = 60;
            var login_timer = setInterval(function () {
                if (times <= 0) {
                    clearInterval(login_timer);
                    Dcat.error('二维码已失效');
                }
                if (scan_login_scene) {
                    appstore_post('login/scan', { 'scene': scan_login_scene }).then(function (res) {
                        if (res.status) {
                            clearInterval(login_timer);
                            appstore_set_cookie('appstore-token', res.data.token);
                            $("#appstore-login-wrapper").css("display", 'none');
                            $("#appstore-main-wrapper").css("display", 'block');
                            $("#search-domain-auth").css("display", 'block');
                            appstore_check_auth();
                            appstore_update_user_balance();
                            Dcat.success(res.data.user.user_name + ' 欢迎回来！');
                        } else {
                            // Dcat.error(res.message);
                        }
                    });
                } else {
                    clearInterval(login_timer);
                }
                times -= 1;
            }, 2000);
        }

        function appstore_login(way = 1) {
            if (way == 1) {
                let token = appstore_get_cookie('appstore-token');
                if (token && token.length > 0) {
                    $("#appstore-main-wrapper").css("display", 'block');
                    $("#search-domain-auth").css("display", 'block');
                    appstore_check_auth();
                    appstore_update_user_balance();
                } else {
                    Dcat.loading(true);
                    appstore_post('login/scan', {}, 'GET').then(function (res) {
                        if (res.status) {
                            $("#appstore-main-wrapper").css("display", 'none');
                            $("#appstore-login-wrapper").css("display", 'flex');
                            $("#search-domain-auth").css("display", 'none');
                            $("#appstore-login-scan-code").attr('src', res.data.url);
                            scan_login_scene = res.data.scene;

                            appstore_login_try();
                        } else {
                            Dcat.error(res.message);
                        }
                    }).finally(function (res) {
                        Dcat.loading(false);
                    });
                }
            } else {
                appstore_set_cookie('appstore-token', '');
                Dcat.success('已退出登录');
                setTimeout(() => {
                    appstore_login(1);
                }, 1000);
            }
        }

        function appstore_download_licene() {
            var search_domain = ($('#domain-auth-search').val() || '{{ $current_domain }}') || localDomain;
            if (search_domain != localDomain && !is_first_search && $('#domain-auth-search').val() == '') {
                search_domain = localDomain;
            }
            appstore_post('three/license', {
                'domain': (search_domain == localDomain ? '' : search_domain)
            }).then(function (res) {
                if (res.status) {
                    window.open(res.data.url, "_blank");
                } else {
                    Dcat.error(res.message);
                }
            }).finally(function (res) {
                Dcat.loading(false);
            });
            return false;
        }

        function appstore_main() {
            appstore_login(1);
        }

        appstore_main();

        $('#domain-auth-search').on('keypress', function (e) {
            if (e.which == 13) { // 检测回车键
                appstore_check_auth();
                return false;
            }
        });

        // 自定义模态框相关函数
        function openCustomModal(message, showInput = false) {
            return new Promise((resolve) => {
                const modal = document.getElementById('custom-modal');
                const messageElement = document.getElementById('custom-modal-message');
                const inputElement = document.getElementById('custom-modal-input');
                const submitButton = document.querySelector('.custom-modal button');

                messageElement.innerText = message;
                inputElement.style.display = showInput ? 'block' : 'none';
                inputElement.value = '';

                modal.style.display = 'flex';

                submitButton.onclick = () => {
                    const userInput = showInput ? inputElement.value : null;
                    closeCustomModal();
                    resolve(userInput);
                };
            });
        }

        function closeCustomModal() {
            const modal = document.getElementById('custom-modal');
            modal.style.display = 'none';
        }
    </script>

    <style>
        .appstore-gray-filter {
            filter: grayscale(1);
            opacity: 0.7;
        }

        .app-center {
            background-color: #fff;
            min-height: 85vh;
            width: 100%;
            padding: 10px;
            position: relative;
        }

        .appstore-mask {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-color: rgba(0, 0, 0, 0.3);
            z-index: 9999999;
        }

        .appstore-main-wrapper .appstore-main-title {
            font-size: 20px;
            font-weight: 600;
        }

        .appstore-main-wrapper .appstore-main-modules-list {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
        }

        .appstore-main-modules-list .appstore-model-item {
            width: 250px;
            height: 170px;
            text-align: center;
            transform: scale(0.9);
            cursor: pointer;
        }

        .appstore-main-modules-list .appstore-model-item .appstore-model-img {
            width: 150px;
            height: 150px;
        }

        .appstore-main-modules-list .appstore-model-item.dashed .appstore-model-img {
            border: 1px dashed red;
        }

        .appstore-main-modules-list .appstore-model-item .appstore-model-name {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        /* 分站部分 */
        .tenant_list_container {
            display: flex;
            gap: 10px;
            padding-left: 50px;
            box-sizing: border-box;
            flex-wrap: wrap;
        }

        .tenant_list_container .tenant_card {
            background-color: rgba(0, 0, 0, 0.1);
            max-height: 100px;
            max-width: 100px;
            min-height: 100px;
            min-width: 100px;
            line-height: 100px;
            flex: 1;
            border-radius: 16px;
            display: flex;
            align-items: center;
            justify-content: center;
            white-space: nowrap;
            margin-bottom: 10px;
        }

        /* 登录界面 */
        .appstore-login-wrapper {
            margin: auto;
            height: 85vh;
            display: flex;
            flex-direction: column;
            justify-content: center;
        }

        .appstore-login-wrapper .inner {
            margin: auto;
            display: flex;
            flex-direction: column;
            justify-content: center;
            text-align: center;
            width: 200px;
            height: 230px;
        }

        .appstore-login-wrapper .code {
            width: 200px;
            height: 200px;
            margin: auto;
        }

        /* 付费购买的点击弹框 */
        .appstore-model-buy-box {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) scale(0.6);
            z-index: 999999999;
            width: 500px;
            height: 920px;
        }

        .appstore-model-buy-box .model-tip {
            display: block;
            font-size: 21px;
            text-align: center;
            max-width: 90%;
            margin: 0 auto 26px;
            color: blue;
        }

        .appstore-model-buy-box .model-card {
            position: relative;
            width: 500px;
            height: 800px;
            background: white;
            transition: .4s ease-in-out;
            border-radius: 15px;
            box-shadow: rgba(0, 0, 0, 0.07) 0px 1px 1px, rgba(0, 0, 0, 0.07) 0px 2px 2px, rgba(0, 0, 0, 0.07) 0px 4px 4px, rgba(0, 0, 0, 0.07) 0px 8px 8px, rgba(0, 0, 0, 0.07) 0px 16px 16px;
            overflow: hidden;
            display: flex;
            flex-direction: column;
            justify-content: center;
            box-sizing: border-box;
        }

        .appstore-model-buy-box .model-heading {
            display: block;
            text-align: center;
            color: black;
            font-weight: bold;
            font-size: 30px;
            margin-bottom: 26px;
        }

        .appstore-model-buy-box .model-details {
            display: block;
            font-size: 25px;
            text-align: center;
            max-width: 80%;
            margin: 0 auto 26px;
        }

        .appstore-model-buy-box .model-price {
            display: block;
            color: black;
            font-weight: bold;
            font-size: 30px;
            text-align: center;
            margin-bottom: 26px;
        }

        .appstore-model-buy-box .model-btn {
            width: 80%;
            position: relative;
            border: none;
            outline: none;
            color: white;
            font-size: 25px;
            border-radius: 10px;
            transition: .4s ease-in-out;
            font-weight: bold;
            padding: 8px 0;
            margin: 0 auto 26px;
            cursor: pointer;
        }

        .appstore-model-buy-box .model-btn0 {
            background-color: #07C160;
        }

        .appstore-model-buy-box .model-btn2 {
            background-color: #01C0AA;
        }

        .appstore-model-buy-box .model-btn1 {
            background-color: #108EE9;
        }

        .appstore-model-buy-box .model-res-balance {
            display: block;
            font-size: 20px;
            text-align: center;
            max-width: 80%;
            margin: 0 auto 26px;
            color: gray;
        }

        .appstore-model-buy-box .model-glasses {
            display: flex;
            justify-content: center;
            margin-bottom: 30px;
        }

        .appstore-model-buy-box .model-glasses .code-img {
            width: 200px;
            height: 200px;
        }

        .custom-modal {
            display: none;
            position: fixed;
            z-index: 9999999999;
            left: 50%;
            top: 50%;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgba(0, 0, 0, 0.6);
            display: flex;
            justify-content: center;
            align-items: center;
            transform: translate(-50%, -50%);
        }

        .custom-modal-content {
            background-color: #fefefe;
            padding: 20px;
            border: 1px solid #888;
            width: 90%;
            max-width: 500px;
            border-radius: 10px;
            box-shadow: 0 5px 15px rgba(0, 0, 0, 0.3);
            position: relative;
            text-align: center;
        }

        .custom-modal-content .close {
            color: #aaa;
            position: absolute;
            right: 20px;
            top: 20px;
            font-size: 28px;
            font-weight: bold;
        }

        .custom-modal-content .close:hover,
        .custom-modal-content .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }

        .custom-modal-content #custom-modal-message {
            font-size: 18px;
            margin-bottom: 20px;
            margin-top: 42px;
        }

        .custom-modal-content input[type="text"] {
            width: calc(100% - 40px);
            padding: 10px;
            margin: 10px auto 20px auto;
            display: block;
            box-sizing: border-box;
            font-size: 18px;
            border: 1px solid #ccc;
            border-radius: 5px;
            text-align: center;
        }

        .custom-modal-content button {
            background-color: #007bff;
            color: white;
            padding: 15px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 18px;
            width: 100%;
            box-sizing: border-box;
            margin-top: 10px;
        }

        .custom-modal-content button:hover {
            background-color: #0056b3;
        }

    </style>
