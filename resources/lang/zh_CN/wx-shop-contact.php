<?php 
return [
    'labels' => [
        'WxShopContact' => '店铺经营地址',
        'wx-shop-contact' => '店铺经营地址',
    ],
    'fields' => [
        'user_id' => '用户id',
        'shop_id' => '店铺id',
        'name' => '联系人',
        'country_code' => '国家区号',
        'phone' => '联系电话',
        'longitude' => '经度',
        'latitude' => '纬度',
        'address_name' => '地址名称',
        'address_detail' => '地址详细',
    ],
    'options' => [
    ],
];
