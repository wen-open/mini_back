<?php 
return [
    'labels' => [
        'WxUsedGood' => '二手商品',
        'wx-used-good' => '二手商品',
    ],
    'fields' => [
        'user_id' => '用户id',
        'title' => '标题',
        'detail' => '内容',
        'classify_id' => '分类id',
        'price' => '价格',
        'original_price' => '原价',
        'is_self_pickup' => '是否自提',
        'is_express_delivery' => '是否通过快递',
        'express_cost' => '运费',
        'image_urls' => '图片数组',
        'longitude' => '经度',
        'latitude' => '纬度',
        'address_name' => '地址名',
        'address_detailed' => '地址名',
        'ip' => 'ip地址',
        'country' => '国家',
        'province' => '省份',
        'city' => '城市',
        'district' => '县城',
        'status' => '状态',
    ],
    'options' => [
    ],
];
