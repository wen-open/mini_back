<?php
return [
    'labels' => [
        'WxCaptchaImg' => '人机验证码库',
        'wx-captcha-img' => '人机验证码库',
    ],
    'fields' => [
        'md5' => 'md5',
        'img' => '图片',
        'status' => '状态'
    ],
    'options' => [
    ],
];
