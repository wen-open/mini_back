<?php
return [
    'labels' => [
        'WxPost' => '文章',
        'wx-post' => '文章',
    ],
    'fields' => [
        'posts_content' => '帖子内容',
        'user_id' => '用户ID',
        'circle_id' => '圈子ID',
        'circle_name' => '所属圈子',
        'address_id' => '位置ID',
        'tags_id' => '标签ID',
        'is_information' => '热榜',
        'is_examine' => '审核状态',
        'pay_content_id' => '付费内容',
        'overrule_content' => '驳回原因',
        'overrule_is_read' => '驳回用户是否已读(0未读，1已读)',
        'posts_state' => '状态',
        'voter_id' => '选票活动id',
        'is_sticky' => '置顶',
        'is_anonymous' => '匿名'
    ],
    'options' => [
    ],
];
