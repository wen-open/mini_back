<?php
return [
    'labels' => [
        'WxMpFreePublish' => '公众号推送',
        'wx-mp-free-publish' => '公众号推送',
    ],
    'fields' => [
        'media_id' => '素材id',
        'title' => '标题',
        'author' => '作者',
        'digest' => '摘要',
        'content' => '内容',
        'content_source_url' => '原文链接',
        'thumb_url' => '封面图片',
        'thumb_media_id' => '图文消息的封面图片素材id',
        'need_open_comment' => '开启评论',
        'only_fans_can_comment' => '是否粉丝才可评论',
        'pic_crop_235_1' => '封面裁剪为2.35:1规格的坐标字段',
        'pic_crop_1_1' => '封面裁剪为1:1规格的坐标字段',
        'add_draft_time' => '同步到草稿',
        'hope_publish_time' => '定时发布',
        'publish_id' => '发布任务id',
        'publish_status' => '发布状态',
        'article_id' => '文章id',
        'article_url' => '查看链接'
    ],
    'options' => [
    ],
];
