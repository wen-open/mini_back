<?php
return [
    'labels' => [
        'WxAttachment' => '附件',
        'wx-attachment' => 'WxAttachment',
    ],
    'fields' => [
        'path' => 'path',
        'cos_review' => 'cos审核结果'
    ],
    'options' => [
    ],
];
