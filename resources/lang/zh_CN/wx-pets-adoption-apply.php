<?php 
return [
    'labels' => [
        'WxPetsAdoptionApply' => '领养申请库',
        'wx-pets-adoption-apply' => '领养申请库',
    ],
    'fields' => [
        'user_id' => '用户id',
        'pet_adoption_id' => '领养id',
        'gender' => '性别',
        'is_experience' => '养宠经验',
        'marital_status' => '婚姻状况',
        'housing_status' => '住房情况',
        'working_status' => '工作状态',
        'job' => '职业',
        'birthday' => '生日',
        'income' => '收入',
        'province' => '省份',
        'city' => '市',
        'district' => '县',
        'mobile' => '手机号',
        'wechatId' => '微信号',
        'remark' => '简介',
        'status' => '申请状态',
    ],
    'options' => [
    ],
];
