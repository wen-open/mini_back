<?php 
return [
    'labels' => [
        'WxUsedComment' => '二手留言库',
        'wx-used-comment' => '二手留言库',
    ],
    'fields' => [
        'user_id' => '用户id',
        'used_good_id' => '二手商品id',
        'comment_id' => '父级id',
        'comment_agent_id' => '回复的用户id',
        'comment_agent_name' => '回复的用户名',
        'comment_content' => '评论内容',
        'comment_img_url' => '评论图片',
        'longitude' => '经度',
        'latitude' => '纬度',
        'ip' => 'ip地址',
        'country' => '国家',
        'province' => '省份',
        'city' => '城市',
        'district' => '县城',
        'comment_state' => '状态：0:审核中，1:审核通过，2:驳回',
    ],
    'options' => [
    ],
];
