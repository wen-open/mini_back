<?php 
return [
    'labels' => [
        'WxShopCoupon' => '优惠劵',
        'wx-shop-coupon' => '优惠劵',
    ],
    'fields' => [
        'type' => '优惠类型',
        'num' => '优惠数额',
        'min_purchase' => '最低购买金额',
        'goods' => '适用特定商品',
        'classifies' => '适用商品分类',
        'shops' => '适用店铺',
        'users' => '适用用户',
        'start_time' => '使用开始时间',
        'end_time' => '使用结束时间',
        'status' => '是否激活',
    ],
    'options' => [
    ],
];
