<?php
return [
    'labels' => [
        'WxUsedClassify' => '二手分类库',
        'wx-used-classify' => '二手分类库',
    ],
    'fields' => [
        'name' => '名字',
        'desc' => '简介',
        'pic' => '分类图',
        'bg_img' => '背景图',
        'order' => '排序',
        'status' => '状态'
    ],
    'options' => [
    ],
];
