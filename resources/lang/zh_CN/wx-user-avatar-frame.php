<?php 
return [
    'labels' => [
        'WxUserAvatarFrame' => '头像框库',
        'wx-user-avatar-frame' => '头像框库',
    ],
    'fields' => [
        'name' => '名称',
        'image_url' => '图像地址',
        'type' => '类型',
        'price' => '价格',
        'description' => '描述',
        'quantity' => '数量',
        'status' => '状态（0：正常，1：下架）',
    ],
    'options' => [
    ],
];
