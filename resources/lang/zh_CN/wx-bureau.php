<?php
return [
    'labels' => [
        'WxBureau' => '活动',
        'wx-bureau' => '活动',
    ],
    'fields' => [
        'user_id' => '用户id',
        'club_id' => '俱乐部id',
        'title' => '标题',
        'intro' => '活动描述',
        'content' => '描述',
        'image_urls' => '图片数组',
        'video_url' => '视频地址',
        'video_cover' => '视频封面',
        'tags' => '标签',
        'service_date_start' => '服务开始日期',
        'service_date_end' => '服务结束日期',
        'service_time_start' => '服务开始时间',
        'service_time_end' => '服务结束时间',
        'longitude' => '经度',
        'latitude' => '纬度',
        'address_name' => '地址名',
        'address_detailed' => '地址名',
        'ip' => 'ip地址',
        'country' => '国家',
        'province' => '省份',
        'city' => '城市',
        'district' => '县城',
        'refund_policy' => '退款政策',
        'exposure_type' => '曝光类型',
        'user_info_fill' => '用户信息收集',
        'order' => '排序',
        'status' => '状态',
    ],
    'options' => [
    ],
];
