<?php
return [
    'labels' => [
        'WxUserWithdrawal' => '提现',
        'wx-user-withdrawal' => '提现',
    ],
    'fields' => [
        'user_id' => 'user_id',
        'price' => '提现金额',
        'pay_amount' => '打款金额',
        'bank_name' => '支行名字',
        'bank_card' => '银行卡号',
        'real_name' => '真实名字',
        'wechat_phone' => '微信手机号',
        'alipay_phone' => '支付宝手机号',
        'refuse_tip' => '异常原因',
        'state' => '状态',
        'account_at' => '到账时间',
        'withdrawal_id' => '提现单号',
        'account_way' => '到账方式',
        'id_card' => '身份证号',
        'wx_batch_id' => '微信批次号',
        'ali_batch_id' => '支付宝批次号'
    ],
    'options' => [
    ],
];
