<?php 
return [
    'labels' => [
        'WxBureauComment' => '组局留言',
        'wx-bureau-comment' => '组局留言',
    ],
    'fields' => [
        'user_id' => '用户id',
        'bureau_id' => '活动id',
        'comment_id' => '父级id',
        'comment_agent_id' => '回复的用户id',
        'comment_agent_name' => '回复的用户名',
        'comment_content' => '评论内容',
        'comment_img_url' => '评论图片',
        'longitude' => '经度',
        'latitude' => '纬度',
        'ip' => 'ip地址',
        'country' => '国家',
        'province' => '省份',
        'city' => '城市',
        'district' => '县城',
        'is_sticky' => '是否置顶',
        'comment_state' => '状态：0:审核中，1:审核通过，2:驳回',
    ],
    'options' => [
    ],
];
