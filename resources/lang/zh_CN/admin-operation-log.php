<?php 
return [
    'labels' => [
        'AdminOperationLog' => '操作日志',
        'admin-operation-log' => '操作日志',
    ],
    'fields' => [
        'user_id' => '管理员id',
        'path' => '请求路径',
        'method' => '请求类型',
        'ip' => '请求ip',
        'input' => '请求参数',
    ],
    'options' => [
    ],
];
