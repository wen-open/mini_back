<?php
return [
    'labels' => [
        'WxUserInviteBenefitRecord' => '用户付费-收益分成记录',
        'wx-user-invite-benefit-record' => '用户付费-收益分成记录',
    ],
    'fields' => [
        'user_id' => '用户id',
        'order_id' => '订单id',
        'shop_order_id' => '商品订单id',
        'order_price' => '订单金额',
        'lv1_user' => '一层上级id',
        'lv1_benefit' => '一级用户收益',
        'lv2_user' => '二层上级id',
        'lv2_benefit' => '二级用户收益',
        'lv3_user' => '三层上级id',
        'lv3_benefit' => '三级用户收益',
    ],
    'options' => [
    ],
];
