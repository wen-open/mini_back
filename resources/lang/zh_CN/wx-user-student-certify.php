<?php
return [
    'labels' => [
        'WxUserStudentCertify' => '学生认证申请表',
        'wx-user-student-certify' => '学生认证申请表',
    ],
    'fields' => [
        'sid' => '学校唯一标识',
        'school' => '学校名字',
        'email' => '学校邮箱',
        'questions' => '答对的题目',
        'picture' => '证件照片',
        'picture2' => '证件照片2',
        'introduce' => '说明',
        'status' => '认证状态',
        'role' => '学校身份',
        'term' => '入学年份'
    ],
    'options' => [
    ],
];
