<?php
return [
    'labels' => [
        'WxVoterPlayer' => '参与选手',
        'wx-voter-player' => '参与选手',
    ],
    'fields' => [
        'user_id' => '用户id',
        'voter_id' => '选票id',
        'relative_id' => '选手展示id',
        'show_name' => '展示名字',
        'work_name' => '作品名字',
        'work_desc' => '作品描述',
        'img_urls' => '图片',
        'video_url' => '视频地址',
        'video_cover' => '视频封面',
        'contact_text' => '联系信息',
        'status' => '状态',
    ],
    'options' => [
    ],
];
