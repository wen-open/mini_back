<?php
return [
    'labels' => [
        'WxPageEvent' => '页面动作',
        'wx-page-event' => '页面动作',
    ],
    'fields' => [
        'scene' => '场景',
        'scene_id' => '场景id',
        'target_type' => '跳转类型',
        'target_id' => '跳转id',
        'interval' => '间隔/分钟'
    ],
    'options' => [
    ],
];
