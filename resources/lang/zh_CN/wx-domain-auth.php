<?php
return [
    'labels' => [
        'WxDomainAuth' => '授权表',
        'wx-domain-auth' => '授权表',
    ],
    'fields' => [
        'user_id' => '用户',
        'domain_1' => '域名1',
        'tenant_1' => '分站1',
        'domain_2' => '域名2',
        'tenant_2' => '分站2',
        'domain_3' => '域名3',
        'tenant_3' => '分站3',
        'domain_4' => '域名4',
        'tenant_4' => '分站4',
        'domain_5' => '域名5',
        'tenant_5' => '分站5',
        'domain_6' => '域名6',
        'tenant_6' => '分站6',
        'domain_7' => '域名7',
        'tenant_7' => '分站7',
        'pays' => '付费模块',
        'who' => '代理人',
    ],
    'options' => [
    ],
];
