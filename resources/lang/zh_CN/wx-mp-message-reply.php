<?php 
return [
    'labels' => [
        'WxMpMessageReply' => '自动回复',
        'wx-mp-message-reply' => '自动回复',
    ],
    'fields' => [
        'word' => '触发词',
        'type' => '匹配关系',
        'mp_message_id' => '回复消息id',
        'order' => '权重（越大越靠前）',
        'status' => '是否启用',
    ],
    'options' => [
    ],
];
