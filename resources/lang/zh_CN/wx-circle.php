<?php
return [
    'labels' => [
        'WxCircle' => '圈子',
        'wx-circle' => '圈子',
    ],
    'fields' => [
        'circle_name' => '圈子名称',
        'sort'=> '排序',
        'circle_introduce' => '圈子介绍',
        'head_portrait' => '圈子头像',
        'background_maps' => '圈子背景',
        'plate_id' => '板块',
        'list_style' => '展示样式',
        'wxPlate' => [
            'plate_name'=>'板块名称'
        ],
        'wxUser'=>[
            'user_name'=>'用户名'
        ],
        'user_id' => '圈主',
        'is_top_recommend' => '圈子页顶部推荐',
        'is_hot' => '热门圈子',
        'is_publish_admin' => '限制管理员发布',
        'circle_state' => '状态',
        'tenant_show' => '展示分站',
        'is_city_select' => '城市选择',
        'is_tenant_select' => '只看本站'
    ],
    'options' => [
    ],
];
