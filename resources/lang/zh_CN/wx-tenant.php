<?php
return [
    'labels' => [
        'WxTenant' => '分站表',
        'wx-tenant' => '分站表',
    ],
    'fields' => [
        'tenant_id' => '分站ID',
        'name' => '名称',
        'short' => '简称',
        'logo' => 'Logo',
        'desc' => '描述',
        'status' => '状态',
        'tip' => '备注',
        'add_province' => '省',
        'add_city' => '市',
        'add_district' => '县',
        'add_detail' => '详细',
        'add_longitude' => '纬度',
        'add_latitude' => '经度',
    ],
    'options' => [
    ],
];
