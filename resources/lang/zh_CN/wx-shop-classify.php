<?php
return [
    'labels' => [
        'WxShopClassify' => 'SHOP分类',
        'wx-shop-classify' => 'WxShopClassify',
    ],
    'fields' => [
        'name' => '分类名称',
        'intro' => '分类介绍',
        'pic' => '分类图',
        'bg_img' => '背景图',
        'parent_id' => '父级id',
        'sort' => '排序',
        'state' => '状态',
        'in_h5' => 'h5',
        'in_app' => 'app',
        'in_mp' => '小程序',
        'guarantee' => '保证金',
        'is_shop_type1' => '个人店可卖',
        'is_shop_type2' => '个体户可卖',
        'is_shop_type3' => '企业店可卖',
        'is_shop_type4' => '旗舰店/专卖店可卖',
        'platform_division' => '平台分成%'
    ],
    'options' => [
    ],
];
