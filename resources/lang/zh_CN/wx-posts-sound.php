<?php
return [
    'labels' => [
        'WxPostsSound' => '音频',
        'wx-posts-sound' => '音频',
    ],
    'fields' => [
        'name' => '音频名称',
        'time' => '音频时长',
        'url' => '音频地址',
        'post_id' => '帖子id',
    ],
    'options' => [
    ],
];
