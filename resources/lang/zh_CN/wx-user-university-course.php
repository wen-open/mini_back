<?php 
return [
    'labels' => [
        'WxUserUniversityCourse' => '用户课程表',
        'wx-user-university-course' => '用户课程表',
    ],
    'fields' => [
        'user_id' => '用户id',
        'sid' => '大学ID',
        'year' => '年份',
        'term' => '第几学期',
        'name' => '课程名称',
        'num' => '课程编号',
        'credit' => '学分',
        'total_hours' => '总学时',
        'lecture_hours' => '讲课时数',
        'category' => '课程类别',
        'teach_method' => '授课方式',
        'teacher' => '授课教师',
        'address' => '授课地点',
        'raw_weeks' => '原始周数',
        'raw_section' => '原始节次',
        'week' => '星期几',
        'section' => '节',
        'section_count' => '连续节数',
        'weeks' => '课程周次',
    ],
    'options' => [
    ],
];
