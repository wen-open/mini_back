<?php
return [
    'labels' => [
        'WxClub' => '俱乐部列表',
        'wx-club' => '俱乐部列表',
    ],
    'fields' => [
        'user_id' => '用户id',
        'avatar' => '头像',
        'name' => '名称',
        'intro' => '简介',
        'assistants' => '店员',
        'longitude' => '经度',
        'latitude' => '纬度',
        'address_name' => '地址名',
        'address_detailed' => '地址名',
        'ip' => 'ip地址',
        'country' => '国家',
        'province' => '省份',
        'city' => '城市',
        'district' => '县城',
        'status' => '状态',
    ],
    'options' => [
    ],
];
