<?php 
return [
    'labels' => [
        'WxMpCustomMenu' => '公众号自定义菜单',
        'wx-mp-custom-menu' => '公众号自定义菜单',
    ],
    'fields' => [
        'parent_id' => '父级id',
        'order' => '排序',
        'type' => '响应动作类型',
        'name' => '菜单标题',
        'key' => '事件key',
        'url' => '网页链接',
        'media_id' => '素材media_id',
        'appid' => '小程序的appid',
        'pagepath' => '小程序的页面路径',
        'article_id' => '发布后article_id',
        'depth' => '层级',
    ],
    'options' => [
    ],
];
