<?php
return [
    'labels' => [
        'WxBureauRefund' => '组局活动退款记录',
        'wx-bureau-refund' => '组局活动退款记录',
    ],
    'fields' => [
        'user_id' => '购买用户',
        'order_good_id' => '订单商品id',
        'order_id' => '订单id',
        'goods_id' => '商品id',
        'product_id' => '规格id',
        'amount' => '退回金额',
    ],
    'options' => [
    ],
];
