<?php
return [
    'labels' => [
        'WxUsedRefund' => '二手商品退款记录',
        'wx-used-refund' => '二手商品退款记录',
    ],
    'fields' => [
        'user_id' => '购买用户',
        'order_good_id' => '订单商品id',
        'order_id' => '订单id',
        'goods_id' => '商品id',
        'product_id' => '规格id',
        'amount' => '退回金额',
    ],
    'options' => [
    ],
];
