<?php
return [
    'labels' => [
        'WxVoterTemplate' => '选票模板',
        'wx-voter-template' => '选票模板',
    ],
    'fields' => [
        'status' => '启用',
        'recommend' => '推荐',
        'sort' => '排序',
        'name' => '模板名称',
        'template_type' => '模板类型',
        'cover' => '封面',
        'banner' => '横幅',
        'main_background_image' => '主背景图像',
        'user_background_image' => '用户背景图像',
        'background_color' => '背景颜色',
        'text_color' => '文本颜色',
        'main_color' => '主要颜色',
        'button_bg_color' => '按钮背景颜色',
        'button_text_color' => '按钮文本颜色',
        'text_navigation_bar_color' => '导航栏颜色',
    ],
    'options' => [
    ],
];
