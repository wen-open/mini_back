<?php
return [
    'labels' => [
        'WxPostsCommentManual' => '延时评论',
        'wx-posts-comment-manual' => '延时评论',
    ],
    'fields' => [
        'posts_id' => '笔记id',
        'expect_user' => '期待谁发布',
        'content' => '评论内容',
        'img' => '评论图片',
        'expect_time' => '期待发表时间',
        'publish_time' => '实际发表时间',
        'status' => '状态',
    ],
    'options' => [
    ],
];
