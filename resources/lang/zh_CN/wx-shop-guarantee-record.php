<?php 
return [
    'labels' => [
        'WxShopGuaranteeRecord' => '保证金记录',
        'wx-shop-guarantee-record' => '保证金记录',
    ],
    'fields' => [
        'shop_id' => '店铺id',
        'num' => '变动数量',
        'title' => '标题',
    ],
    'options' => [
    ],
];
