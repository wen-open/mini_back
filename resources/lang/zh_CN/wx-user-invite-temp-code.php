<?php
return [
    'labels' => [
        'WxUserInviteTempCode' => '推广码',
        'wx-user-invite-temp-code' => '推广码',
    ],
    'fields' => [
        'type' => '类型',
        'code' => '推广码',
        'path' => '打开页面',
        'scene' => '携带参数',
        'with_target_type' => '携带动作类型',
        'with_target_id' => '携带动作跳转id',
        'user_id' => '关联用户',
        'shop_id' => '关联店铺',
        'club_id' => '关联俱乐部',
        'circle_id' => '关联圈子',
        'tag_id' => '关联话题',
        'voter_id' => '关联选票',
        'tenant_id' => '进入分站',
        'status' => '状态',
    ],
    'options' => [
    ],
];
