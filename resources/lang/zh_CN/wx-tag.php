<?php
return [
    'labels' => [
        'WxTag' => '话题',
        'wx-tag' => '话题',
    ],
    'fields' => [
        'user_id' => '用户id',
        'tags_name' => '话题名称',
        'tag_introduce' => '描述',
        'head_portrait' => '头像',
        'background_maps' => '移动端背景',
        'pc_background_maps' => 'pc端背景',
        'sort'=> '排序',
        'tags_number' => '使用次数',
        'tags_state' => '状态',
    ],
    'options' => [
    ],
];
