<?php 
return [
    'labels' => [
        'WxCircleTag' => '圈子话题',
        'wx-circle-tag' => '圈子话题',
    ],
    'fields' => [
        'circle_id' => '圈子id',
        'tag_id' => '话题id',
        'times' => '次数',
    ],
    'options' => [
    ],
];
