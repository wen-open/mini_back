<?php
return [
    'labels' => [
        'WxMpMaterial' => '公众号素材库',
        'wx-mp-material' => '公众号素材库',
    ],
    'fields' => [
        'msgtype' => '消息类型',
        'content' => '消息内容',
        'img_url' => '图片url',
        'video_url' => '视频url',
        'voice_url' => '语音url',
        'thumb_url' => '封面url',
        'musicurl' => '音乐地址',
        'media_id' => '素材id',
        'thumb_media_id' => '封面素材id',
        'title' => '标题',
        'description' => '描述',
        'hqmusicurl' => '高质音乐地址',
        'url' => '图文外链',
        'article_id' => '图文article_id',
        'card_id' => '卡卷id',
    ],
    'options' => [
    ],
];
