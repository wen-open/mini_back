<?php 
return [
    'labels' => [
        'WxUniversity' => '2023全国高校库',
        'wx-university' => '2023全国高校库',
    ],
    'fields' => [
        'order' => '倒排序',
        'name' => '学校名字',
        'charge' => '主管部门',
        'location' => '所在地',
        'email_suffix' => '邮箱后缀',
        'sem_start_date_1' => '第一周第一天(一)',
        'sem_end_date_1' => '最后一周最后一天(一)',
        'sem_start_date_2' => '第一周第一天(二)',
        'sem_end_date_2' => '最后一周最后一天(二)',
        'class_time' => '上课时间设定',
        'level' => '办学层次',
        'private' => '是否民办',
        'is_certify' => '是否可被认证',
        'course_user_count' => '课程人数',
    ],
    'options' => [
    ],
];
