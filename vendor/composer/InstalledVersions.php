<?php











namespace Composer;

use Composer\Autoload\ClassLoader;
use Composer\Semver\VersionParser;








class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'laravel/laravel',
  ),
  'versions' => 
  array (
    'bacon/bacon-qr-code' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '448ee9929aece0e86f0e2b926e636f9b53d03ce1',
    ),
    'box/spout' => 
    array (
      'pretty_version' => 'v3.3.0',
      'version' => '3.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bdb027d312b732515b884a341c0ad70372c6295',
    ),
    'brick/math' => 
    array (
      'pretty_version' => '0.11.0',
      'version' => '0.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0ad82ce168c82ba30d1c01ec86116ab52f589478',
    ),
    'carbonphp/carbon-doctrine-types' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '99f76ffa36cce3b70a4a6abce41dba15ca2e84cb',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '3.4.0',
      'version' => '3.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '35e8d0af4486141bc745f23a29cc2091eb624a32',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'dcat/easy-excel' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '20ee838b07f1f5d9c075b84e6f4807cbb21c44b0',
    ),
    'dflydev/dot-access-data' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f41715465d65213d644d3141a6a93081be5d3549',
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1ca8f21980e770095a31456042471a57bc4c68fb',
    ),
    'doctrine/dbal' => 
    array (
      'pretty_version' => '3.8.2',
      'version' => '3.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a19a1d05ca211f41089dffcc387733a6875196cb',
    ),
    'doctrine/deprecations' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dfbaa3c2d2e9a9df1118213f3b8b0c597bb99fab',
    ),
    'doctrine/event-manager' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '95aa4cb529f1e96576f3fda9f5705ada4056a520',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.10',
      'version' => '2.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '5817d0659c5b50c9b950feb9af7b9668e2c436bc',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0a0fa9780f5d4e507415a065172d26a98d02047b',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '861c870e8b75f7c8f69c146c7f89cc1c0f1b49b6',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v3.3.1',
      'version' => '3.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be85b3f05b46c39bbc0d95f6c071ddff669510fa',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.2.6',
      'version' => '3.2.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e5997fa97e8790cdae03a9cbd5e78e45e3c7bda7',
    ),
    'elastic/transport' => 
    array (
      'pretty_version' => 'v8.8.0',
      'version' => '8.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cdf9f63a16ec6bfb4c881ab89aa0e2a61fb7c20b',
    ),
    'elasticsearch/elasticsearch' => 
    array (
      'pretty_version' => 'v8.8.1',
      'version' => '8.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7f4ccfe9b11c92191bc06119848efa115a22924c',
    ),
    'ezyang/htmlpurifier' => 
    array (
      'pretty_version' => 'v4.17.0',
      'version' => '4.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbc513d79acf6691fa9cf10f192c90dd2957f18c',
    ),
    'fakerphp/faker' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bfb4fe148adbf78eff521199619b93a52ae3554b',
    ),
    'fideloper/proxy' => 
    array (
      'pretty_version' => '4.4.2',
      'version' => '4.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a751f2bc86dd8e6cfef12dc0cbdada82f5a18750',
    ),
    'filp/whoops' => 
    array (
      'pretty_version' => '2.15.4',
      'version' => '2.15.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a139776fa3f5985a50b509f2a02ff0f709d2a546',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v6.10.0',
      'version' => '6.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a49db6f0a5033aef5143295342f1c95521b075ff',
    ),
    'fruitcake/php-cors' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3d158f36e7875e2f040f37bc0573956240a5a38b',
    ),
    'graham-campbell/result-type' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fbd48bce38f73f8a4ec8583362e732e4095e5862',
    ),
    'guzzlehttp/command' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0eebc653784f4902b3272e826fe8e88743d14e77',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.8.1',
      'version' => '7.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '41042bc7ab002487b876a0683fc8dce04ddce104',
    ),
    'guzzlehttp/guzzle-services' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bcab7c0d61672b606510a6fe5af3039d04968c0f',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bbff78d96034045e58e13dedd6ad91b5d1253223',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.6.2',
      'version' => '2.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '45b30f99ac27b5ca93cb4831afe16285f57b8221',
    ),
    'guzzlehttp/uri-template' => 
    array (
      'pretty_version' => 'v1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ecea8feef63bd4fef1f037ecb288386999ecc11c',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
    ),
    'handcraftedinthealps/elasticsearch-dsl' => 
    array (
      'pretty_version' => '8.0.2',
      'version' => '8.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '02c457f748ef2d046a338bf8c21a9a9b31327164',
    ),
    'illuminate/auth' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/broadcasting' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/bus' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/cache' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/collections' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/conditionable' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/config' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/console' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/container' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/contracts' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/cookie' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/database' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/encryption' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/events' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/hashing' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/http' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/log' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/macroable' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/mail' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/notifications' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/pagination' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/pipeline' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/queue' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/redis' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/routing' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/session' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/support' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/testing' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/translation' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/validation' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'illuminate/view' => 
    array (
      'replaced' => 
      array (
        0 => 'v9.2.0',
      ),
    ),
    'intervention/image' => 
    array (
      'pretty_version' => '2.7.2',
      'version' => '2.7.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '04be355f8d6734c826045d02a1079ad658322dad',
    ),
    'jetbrains/phpstorm-stubs' => 
    array (
      'pretty_version' => 'v2022.2',
      'version' => '2022.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '01006d9854679672fc8b85c6d5063ea6f25226ac',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'laravel/framework' => 
    array (
      'pretty_version' => 'v9.2.0',
      'version' => '9.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '13372872bed31ae75df8709b9de5cde01d50646e',
    ),
    'laravel/laravel' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'laravel/sail' => 
    array (
      'pretty_version' => 'v1.27.2',
      'version' => '1.27.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '2276a8d9d6cfdcaad98bf67a34331d100149d5b6',
    ),
    'laravel/scout' => 
    array (
      'pretty_version' => 'v9.8.1',
      'version' => '9.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '38595717b396ce733d432b82e3225fa4e0d6c8ef',
    ),
    'laravel/serializable-closure' => 
    array (
      'pretty_version' => 'v1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '3dbf8a8e914634c48d389c1234552666b3d43754',
    ),
    'laravel/socialite' => 
    array (
      'pretty_version' => 'v5.12.1',
      'version' => '5.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7dae1b072573809f32ab6dcf4aebb57c8b3e8acf',
    ),
    'laravel/tinker' => 
    array (
      'pretty_version' => 'v2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '502e0fe3f0415d06d5db1f83a472f0f3b754bafe',
    ),
    'lcobucci/clock' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fb533e093fd61321bfcbac08b131ce805fe183d3',
    ),
    'lcobucci/jwt' => 
    array (
      'pretty_version' => '4.3.0',
      'version' => '4.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7de2fe0d51a96418c0d04004986e410e87f6b4',
    ),
    'league/commonmark' => 
    array (
      'pretty_version' => '2.4.2',
      'version' => '2.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '91c24291965bd6d7c46c46a12ba7492f83b1cadf',
    ),
    'league/config' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '754b3604fb2984c71f4af4a9cbe7b57f346ec1f3',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '3.24.0',
      'version' => '3.24.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b25a361508c407563b34fac6f64a8a17a8819675',
    ),
    'league/flysystem-local' => 
    array (
      'pretty_version' => '3.23.1',
      'version' => '3.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b884d2bf9b53bb4804a56d2df4902bb51e253f00',
    ),
    'league/mime-type-detection' => 
    array (
      'pretty_version' => '1.15.0',
      'version' => '1.15.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ce0f4d1e8a6f4eb0ddff33f57c69c50fd09f4301',
    ),
    'league/oauth1-client' => 
    array (
      'pretty_version' => 'v1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd6365b901b5c287dd41f143033315e2f777e1167',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3fa72e4c71a43f9e9118752a5c90e476a8dc9eb3',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '95c56caa1cf5c766ad6d65b6344b807c1e8405b9',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '728434227fe21be27ff6d86621a1b13107a2562c',
    ),
    'matchish/laravel-scout-elasticsearch' => 
    array (
      'pretty_version' => 'v7.5.0',
      'version' => '7.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '00e3caf4b984e4884358bc861e30d1ac079714a8',
    ),
    'milon/barcode' => 
    array (
      'pretty_version' => 'v10.0.1',
      'version' => '10.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e643a713466f0109aa3ad7d29dae4900444187a5',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '1.6.7',
      'version' => '1.6.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cc058854b3195ba21dc6b1f7b1f60f4ef3a9c06',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.9.2',
      'version' => '2.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '437cb3628f4cf6042cc10ae97fc2b8472e48ca1f',
    ),
    'mtdowling/cron-expression' => 
    array (
      'replaced' => 
      array (
        0 => '^1.0',
      ),
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.11.1',
      'version' => '1.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7284c22080590fb39f2ffa3e9057f10a4ddd0e0c',
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.8.4',
      'version' => '1.8.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a867478eae49c9f59ece437ae7f9506bfaa27483',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.72.3',
      'version' => '2.72.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0c6fd108360c562f6e4fd1dedb8233b423e91c83',
    ),
    'nette/schema' => 
    array (
      'pretty_version' => 'v1.2.5',
      'version' => '1.2.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '0462f0166e823aad657c9224d0f849ecac1ba10a',
    ),
    'nette/utils' => 
    array (
      'pretty_version' => 'v4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd3ad0aa3b9f934602cb3e3902ebccf10be34d218',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.18.0',
      'version' => '4.18.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1bcbb2179f97633e98bbbc87044ee2611c7d7999',
    ),
    'nunomaduro/collision' => 
    array (
      'pretty_version' => 'v6.4.0',
      'version' => '6.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f05978827b9343cba381ca05b8c7deee346b6015',
    ),
    'ongr/elasticsearch-dsl' => 
    array (
      'replaced' => 
      array (
        0 => '8.0.2',
      ),
    ),
    'overtrue/easy-sms' => 
    array (
      'pretty_version' => '2.5.1',
      'version' => '2.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c61a6dcc261e67f5b7f25d7a1b233351482325da',
    ),
    'overtrue/pinyin' => 
    array (
      'pretty_version' => '5.3.0',
      'version' => '5.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd40f248239231ae2d1f94348c323a40c7fc58b1',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
    ),
    'php-http/async-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '*',
      ),
    ),
    'php-http/discovery' => 
    array (
      'pretty_version' => '1.19.2',
      'version' => '1.19.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '61e1a1eb69c92741f5896d9e05fb8e9d7e8bb0cb',
    ),
    'php-http/httplug' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '625ad742c360c8ac580fcc647a1541d29e257f67',
    ),
    'php-http/promise' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2916a606d3b390f4e9e8e2b8dd68581508be0f07',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fde2ccf55eaef7e86021ff1acce26479160a0fa0',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.9.2',
      'version' => '1.9.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '80735db690fe4fc5c76dfa7f9b770634285fa820',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '9.2.30',
      'version' => '9.2.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca2bd87d2f9215904682a9cb9bb37dda98e76089',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '3.0.6',
      'version' => '3.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf1c2e7c203ac650e352f4cc675a7021e7d1b3cf',
    ),
    'phpunit/php-invoker' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '5.0.3',
      'version' => '5.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '9.6.16',
      'version' => '9.6.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '3767b2c56ce02d01e3491046f33466a1ae60a37f',
    ),
    'psr/cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aa5030cfa5405eccfdcb1083ce040c2cb8d253bf',
    ),
    'psr/clock' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e41a24703d4560fd0acb709162f73b8adfc3aa0d',
    ),
    'psr/clock-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1|2.0',
      ),
    ),
    'psr/event-dispatcher' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bb5906edc1c324c9a05aa0873d40117941e5fa90',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '*',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '*',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.1',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
        1 => '*',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe5ea303b0887d5caefd3d431c3e61ad47037001',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
        1 => '1.0|2.0|3.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '764e0b3939f5ca87cb904f570ef9be2d78a07865',
    ),
    'psr/simple-cache-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0|3.0',
      ),
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.12.0',
      'version' => '0.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '750bf031a48fd07c673dbe3f11f72362ea306d0d',
    ),
    'qcloud/cos-sdk-v5' => 
    array (
      'pretty_version' => 'v2.6.9',
      'version' => '2.6.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '85e11f94ff4e13f3f866c4720902e991221b8baa',
    ),
    'qcloud_sts/qcloud-sts-sdk' => 
    array (
      'pretty_version' => '3.0.12',
      'version' => '3.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '16ebb03f1079ef71c272180fc015cc809ce8320b',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ramsey/collection' => 
    array (
      'pretty_version' => '1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad7475d1c9e70b190ecffc58f2d989416af339b4',
    ),
    'ramsey/uuid' => 
    array (
      'pretty_version' => '4.7.5',
      'version' => '4.7.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f0df49ae5ad6efb7afa69e6bfab4e5b1e080d8e',
    ),
    'rayswoole/querylist' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '95bc3bc9f8fcd620f325c711ff1a85a6841ecffe',
    ),
    'rhumsaa/uuid' => 
    array (
      'replaced' => 
      array (
        0 => '4.7.5',
      ),
    ),
    'roave/better-reflection' => 
    array (
      'pretty_version' => '5.11.1',
      'version' => '5.11.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '84cdf580747c3d55238be28d5681e24e7d11e065',
    ),
    'roave/signature' => 
    array (
      'pretty_version' => '1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f92ce20f82c9a1df3b50fc56fbdaeb82cf4c9c5b',
    ),
    'sebastian/cli-parser' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
    ),
    'sebastian/code-unit' => 
    array (
      'pretty_version' => '1.0.8',
      'version' => '1.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '4.0.8',
      'version' => '4.0.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa0f136dd2334583309d32b62544682ee972b51a',
    ),
    'sebastian/complexity' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '25f207c40d62b8b7aa32f5ab026c53561964053a',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '4.0.5',
      'version' => '4.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '74be17022044ebaaecfdf0c5cd504fc9cd5a7131',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '5.1.5',
      'version' => '5.1.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '830c43a844f1f8d5b7a1f6d6076b784454d8b7ed',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '4.0.5',
      'version' => '4.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac230ed27f0f98f597c8a2b6eb7ac563af5e5b9d',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '5.0.6',
      'version' => '5.0.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bde739e7565280bda77be70044ac1047bc007e34',
    ),
    'sebastian/lines-of-code' => 
    array (
      'pretty_version' => '1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1e4a170560925c26d424b6a03aed157e7dcc5c5',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '4.0.4',
      'version' => '4.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '2.0.4',
      'version' => '2.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '4.0.5',
      'version' => '4.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e75bd0f07204fec2a0af9b0f3cfe97d05f92efc1',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
    ),
    'sebastian/type' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '75e2c2a32f5e0b3aef905b9ed0b179b953b3d7c7',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
    ),
    'simplesoftwareio/simple-qrcode' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '17c5e45c79c40f717d4bc08cf5e568f29ebf9333',
    ),
    'socialiteproviders/apple' => 
    array (
      'pretty_version' => '5.6.1',
      'version' => '5.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e00ff7c06e4df297aaeace4e454b2054d6bebe95',
    ),
    'socialiteproviders/manager' => 
    array (
      'pretty_version' => 'v4.5.1',
      'version' => '4.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a67f194f0f4c4c7616c549afc697b78df9658d44',
    ),
    'socialiteproviders/qq' => 
    array (
      'pretty_version' => '4.2.0',
      'version' => '4.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1c46151db25d15f7f1578319cebbd6b8a0e18c8',
    ),
    'socialiteproviders/wechat-service-account' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '56d1d4bb3972b2b30da9db2c0a2bda9b01cd3f46',
    ),
    'socialiteproviders/wechat-web' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0cc9d25c80ee51eba5cb7dfe4b4df8f7822d6c09',
    ),
    'socialiteproviders/weixin' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4f5ca3ee25e1a46e0dc6bb00bd6ba77f344e340f',
    ),
    'socialiteproviders/weixin-web' => 
    array (
      'pretty_version' => '4.1.0',
      'version' => '4.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3862d3a2e3e55bb0381efd0b73958bfc4dffd7b3',
    ),
    'spatie/backtrace' => 
    array (
      'pretty_version' => '1.5.3',
      'version' => '1.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '483f76a82964a0431aa836b6ed0edde0c248e3ab',
    ),
    'spatie/eloquent-sortable' => 
    array (
      'pretty_version' => '4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '64a3365c0d5a7b4a1837b2f29d01ee4c578c416a',
    ),
    'spatie/flare-client-php' => 
    array (
      'pretty_version' => '1.4.4',
      'version' => '1.4.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '17082e780752d346c2db12ef5d6bee8e835e399c',
    ),
    'spatie/ignition' => 
    array (
      'pretty_version' => '1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b6f801c605a593106b623e45ca41496a6e7d56d',
    ),
    'spatie/laravel-ignition' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '29deea5d9cf921590184be6956e657c4f4566440',
    ),
    'spatie/laravel-package-tools' => 
    array (
      'pretty_version' => '1.12.1',
      'version' => '1.12.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '09f80fa240d44fafb1c70657c74ee44ffa929357',
    ),
    'stella-maris/clock' => 
    array (
      'pretty_version' => '0.1.7',
      'version' => '0.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fa23ce16019289a18bb3446fdecd45befcdd94f8',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3ebc83d031b71c39da318ca8b7a07ecc67507ed',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1d00bddb83a4cb2138564b2150001cb6ce272b1',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7df52182f43a68522756ac31a532dd5b1e6db67',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '2eaf8e63bc5b8cefabd4a800157f0d0c094f677a',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7bc61cc2db649b4637d331240c5346dcc7708051',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.0|3.0',
      ),
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '5cc9cac6586fc0c28cd173780ca696e419fefa11',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v6.0.20',
      'version' => '6.0.20.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e16b2676a4b3b1fa12378a20b29c364feda2a8d6',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v6.0.20',
      'version' => '6.0.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '6dc70833fd0ef5e861e17c7854c12d7d86679349',
    ),
    'symfony/mailer' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd60799210c488f545ddde2444dc1aa548322872',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd7052547a0070cbeadd474e172b527a00d657301',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ef4d7e442ca910c4764bce785146269b30cb5fc4',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '32a9da87d7b3245e09ac426c83d334ae9f06f80f',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a287ed7475f85bf6f61890146edbc932c0fff919',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bc45c394692b948b4d383a08d7753968bed9a83d',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9773676c8a1bb1f8d4340a62efe641cf76eda7ec',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '861391a8da9a04cbad2d232ddd9e4893220d6e25',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '87b68208d5c1188808dd7839ee1e6c8ec3b02f1b',
    ),
    'symfony/polyfill-php81' => 
    array (
      'pretty_version' => 'v1.29.0',
      'version' => '1.29.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c565ad1e63f30e7477fc40738343c62b40bc672d',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '2114fd60f26a296cc403a7939ab91478475a33d4',
    ),
    'symfony/psr-http-message-bridge' => 
    array (
      'pretty_version' => 'v2.3.1',
      'version' => '2.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '581ca6067eb62640de5ff08ee1ba6850a0ee472e',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e56ca9b41c1ec447193474cd86ad7c0b547755ac',
    ),
    'symfony/serializer' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '6aff9e2894ff0dcfa7aca38f9d0396ed65627d07',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd78d39c1599bd1188b8e26bb341da52c3c6d8a66',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd9e72497367c23e08bf94176d2be45b00a9d232a',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => '9c24b3fdbbe9fb2ef3a6afd8bbaadfd72dad681f',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eb980457fa6899840fe1687e8627a03a7d8a3d52',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v6.0.19',
      'version' => '6.0.19.0',
      'aliases' => 
      array (
      ),
      'reference' => 'deec3a812a0305a50db8ae689b183f43d915c884',
    ),
    'thenorthmemory/xml' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f50c63450a0b098772423f8bdc3c4ad2c4c30bb',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.2',
      'version' => '1.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b2ad5003ca10d4ee50a12da31de12a5774ba6b96',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => 'v2.2.7',
      'version' => '2.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '83ee6f38df0a63106a9e4536e3060458b74ccedb',
    ),
    'topthink/think-helper' => 
    array (
      'pretty_version' => 'v3.1.6',
      'version' => '3.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v5.6.0',
      'version' => '5.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2cf9fb6054c2bb1d59d1f3817706ecdb9d2934c4',
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.11.0',
      'version' => '1.11.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
    ),
    'yansongda/pay' => 
    array (
      'pretty_version' => 'v3.5.3',
      'version' => '3.5.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '6db99832ae2d52c4aa563ca6f2759ec1cf3dcde0',
    ),
    'yansongda/supports' => 
    array (
      'pretty_version' => 'v4.0.9',
      'version' => '4.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3479723be665360a5635c15f184a1d0a8dc995a',
    ),
  ),
);
private static $canGetVendors;
private static $installedByVendor = array();







public static function getInstalledPackages()
{
$packages = array();
foreach (self::getInstalled() as $installed) {
$packages[] = array_keys($installed['versions']);
}

if (1 === \count($packages)) {
return $packages[0];
}

return array_keys(array_flip(\call_user_func_array('array_merge', $packages)));
}









public static function isInstalled($packageName)
{
foreach (self::getInstalled() as $installed) {
if (isset($installed['versions'][$packageName])) {
return true;
}
}

return false;
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

$ranges = array();
if (isset($installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = $installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', $installed['versions'][$packageName])) {
$ranges = array_merge($ranges, $installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['version'])) {
return null;
}

return $installed['versions'][$packageName]['version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getPrettyVersion($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return $installed['versions'][$packageName]['pretty_version'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getReference($packageName)
{
foreach (self::getInstalled() as $installed) {
if (!isset($installed['versions'][$packageName])) {
continue;
}

if (!isset($installed['versions'][$packageName]['reference'])) {
return null;
}

return $installed['versions'][$packageName]['reference'];
}

throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}





public static function getRootPackage()
{
$installed = self::getInstalled();

return $installed[0]['root'];
}








public static function getRawData()
{
@trigger_error('getRawData only returns the first dataset loaded, which may not be what you expect. Use getAllRawData() instead which returns all datasets for all autoloaders present in the process.', E_USER_DEPRECATED);

return self::$installed;
}







public static function getAllRawData()
{
return self::getInstalled();
}



















public static function reload($data)
{
self::$installed = $data;
self::$installedByVendor = array();
}





private static function getInstalled()
{
if (null === self::$canGetVendors) {
self::$canGetVendors = method_exists('Composer\Autoload\ClassLoader', 'getRegisteredLoaders');
}

$installed = array();

if (self::$canGetVendors) {
foreach (ClassLoader::getRegisteredLoaders() as $vendorDir => $loader) {
if (isset(self::$installedByVendor[$vendorDir])) {
$installed[] = self::$installedByVendor[$vendorDir];
} elseif (is_file($vendorDir.'/composer/installed.php')) {
$installed[] = self::$installedByVendor[$vendorDir] = require $vendorDir.'/composer/installed.php';
}
}
}

$installed[] = self::$installed;

return $installed;
}
}
