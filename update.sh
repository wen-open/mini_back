#!/bin/bash

# 安装git函数
install_git() {
    echo "正在安装git..."
    # 执行安装git的命令，例如：apt-get install git -y
    # 请根据你的Linux发行版和包管理器进行相应的修改
    yum install git -y
}

# 安装rsync函数
install_rsync() {
    echo "正在安装rsync..."
    # 执行安装rsync的命令，例如：apt-get install rsync -y
    # 请根据你的Linux发行版和包管理器进行相应的修改
    yum install rsync -y
}

# 检查git是否安装
if ! command -v git &> /dev/null; then
    install_git
fi

# 检查rsync是否安装
if ! command -v rsync &> /dev/null; then
    install_rsync
fi

echo "----------------------------------------------"
# 设置要克隆的项目的URL
git_url="https://gitee.com/wen-open/mini_back"
rm -rf mini_back
rm -rf front
rm -rf public/storage/qrcode
rm -rf storage/tmp
# 首先进行git clone操作
echo "开始执行git clone操作..."
git clone $git_url

# 检查克隆是否成功
if [ $? -eq 0 ]; then
    echo "git clone操作成功！"
else
    echo "git clone操作失败！"
    exit 1
fi

# 获取克隆的项目名称
repository_name=$(basename $git_url .git)

# 检查是否存在同名文件夹
if [ -d "$repository_name" ]; then
    # 如果同名文件夹存在，则将其剪切到当前目录
    echo "开始剪切项目到当前目录..."
    rm -rf $repository_name/public/storage/logos
    rm -rf $repository_name/public/storage/tarbar
    rsync -zqr $repository_name/* ./
    rsync -zqr $repository_name/.git ./
    rm -rf $repository_name
    echo "项目已成功剪切到当前目录！"
else
    echo "克隆的项目文件夹不存在！"
    exit 1
fi
# 清理下载的git仓库

echo "----------------------------------------------"
chmod -R 755 ./*
echo "文件权限设置755权限！"
echo "----------------------------------------------"
# 更改文件所有者为www用户
chown -R www:www ./*
echo "文件所有者已更改www用户！"
echo "----------------------------------------------"
# 检查目录是否存在
if [ ! -d "bootstrap/cache" ]; then
    echo "bootstrap/cache 目录不存在！"
    exit 1
fi

# 删除 packages.php 文件
if [ -f "bootstrap/cache/packages.php" ]; then
    echo "开始删除 bootstrap/cache/packages.php..."
    rm -f bootstrap/cache/packages.php
    echo "bootstrap/cache/packages.php 已成功删除！"
else
    echo "bootstrap/cache/packages.php 文件不存在！"
fi

# 删除 services.php 文件
if [ -f "bootstrap/cache/services.php" ]; then
    echo "开始删除 bootstrap/cache/services.php..."
    rm -f bootstrap/cache/services.php
    echo "bootstrap/cache/services.php 已成功删除！"
else
    echo "bootstrap/cache/services.php 文件不存在！"
fi

echo "----------------------------------------------"
chmod -R 766 storage
echo "文件夹storage权限更改完成！"
echo "----------------------------------------------"
chmod -R 766 public
echo "文件夹public权限更改完成！"
echo "----------------------------------------------"
chmod -R 766 app/Extensions
echo "文件夹app/Extensions权限更改完成！"
echo "----------------------------------------------"
rm -rf .git

echo "脚本执行完毕！"
